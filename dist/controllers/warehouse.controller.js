"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WarehouseController = void 0;
const routing_controllers_1 = require("routing-controllers");
const warehouse_service_1 = require("../services/warehouse.service");
const utils_1 = require("../helpers/utils");
const shared_service_1 = require("../services/shared.service");
const auth_service_1 = require("../services/auth.service");
const role_service_1 = require("../services/role.service");
const subscription_service_1 = require("../services/subscription.service");
const validation_interface_1 = require("../interface/validation.interface");
let WarehouseController = class WarehouseController {
    constructor(warehouseService, sharedService, authService, roleService, subscriptionService) {
        this.warehouseService = warehouseService;
        this.sharedService = sharedService;
        this.authService = authService;
        this.roleService = roleService;
        this.subscriptionService = subscriptionService;
    }
    async createWarehouse(req) {
        const isUserExist = await this.sharedService.checkUserExist(req.ownerEmailId);
        if (isUserExist) {
            throw new routing_controllers_1.UnauthorizedError('Email Id already exist');
        }
        else {
            const roleId = await this.roleService.getRoleIdByRole((req.role).toUpperCase());
            if (roleId) {
                const encryptPassword = await utils_1.Utils.encryptPassword(req.password);
                const saveAuth = await this.authService.createAuth(req.ownerEmailId, roleId, encryptPassword);
                if (saveAuth) {
                    const createWarehouse = await this.warehouseService.createWarehouse(req, saveAuth._id);
                    if (createWarehouse) {
                        return JSON.parse(JSON.stringify({
                            message: 'Warehouse created successfully',
                            data: createWarehouse
                        }));
                    }
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to create warehouse due to server error');
                }
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to create warehouse due to server error');
            }
        }
    }
    async getWareHouse(id) {
        const warehouse = await this.warehouseService.getWarehouse(id);
        if (warehouse) {
            if (warehouse) {
                return {
                    message: 'Warehouse fetch successfully',
                    data: JSON.parse(JSON.stringify(warehouse))
                };
            }
            else {
                throw new routing_controllers_1.NotFoundError('Warehouse not found');
            }
        }
    }
    async checkActiveWarehouse(id) {
        const checkStatus = await this.subscriptionService.getSubscription(undefined, id, undefined);
        if (checkStatus.length > 0) {
            return {
                status: 'active',
            };
        }
        else {
            return {
                status: 'expired'
            };
        }
    }
    async updateWarehouse(req) {
        const vendor = await this.warehouseService.updateWarehouse(req);
        if (vendor) {
            return {
                message: 'Vendor updated Successfully',
                data: JSON.parse(JSON.stringify(vendor))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('Vendor not found');
        }
    }
    async deleteWarehouse(req) {
        const role = await this.roleService.getRoleIdByRole(('WAREHOUSE').toUpperCase());
        const authData = {
            _id: req.auth,
            roleId: role,
            isDeleted: true
        };
        const deleteAuth = await this.authService.deleteAuth(authData);
        if (deleteAuth) {
            const vendor = await this.warehouseService.deleteWarehouse(req);
            if (vendor) {
                return {
                    message: 'Vendor deleted Successfully',
                    data: JSON.parse(JSON.stringify(vendor))
                };
            }
            else {
                throw new routing_controllers_1.NotFoundError('vendor not found');
            }
        }
        else {
            throw new routing_controllers_1.InternalServerError('Not able to remove Vendor now please try later');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Warehouse]),
    __metadata("design:returntype", Promise)
], WarehouseController.prototype, "createWarehouse", null);
__decorate([
    routing_controllers_1.Get('/'),
    routing_controllers_1.Authorized(),
    __param(0, routing_controllers_1.QueryParam('warehouseId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], WarehouseController.prototype, "getWareHouse", null);
__decorate([
    routing_controllers_1.Get('/check-active'),
    routing_controllers_1.Authorized(),
    __param(0, routing_controllers_1.QueryParam('warehouseId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], WarehouseController.prototype, "checkActiveWarehouse", null);
__decorate([
    routing_controllers_1.Put('/update'),
    routing_controllers_1.Authorized(),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.UpdateWarehouse]),
    __metadata("design:returntype", Promise)
], WarehouseController.prototype, "updateWarehouse", null);
__decorate([
    routing_controllers_1.Put('/delete'),
    routing_controllers_1.Authorized(),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.UpdateWarehouse]),
    __metadata("design:returntype", Promise)
], WarehouseController.prototype, "deleteWarehouse", null);
WarehouseController = __decorate([
    routing_controllers_1.JsonController('/warehouse'),
    __metadata("design:paramtypes", [warehouse_service_1.WarehouseService,
        shared_service_1.SharedService,
        auth_service_1.AuthService,
        role_service_1.RoleService,
        subscription_service_1.SubscriptionService])
], WarehouseController);
exports.WarehouseController = WarehouseController;
//# sourceMappingURL=warehouse.controller.js.map