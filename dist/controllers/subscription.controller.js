"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionController = void 0;
const routing_controllers_1 = require("routing-controllers");
const validation_interface_1 = require("../interface/validation.interface");
const subscription_service_1 = require("../services/subscription.service");
const warehouse_service_1 = require("../services/warehouse.service");
const offer_service_1 = require("../services/offer.service");
let SubscriptionController = class SubscriptionController {
    constructor(subscriptionService, warehouseService, offerService) {
        this.subscriptionService = subscriptionService;
        this.warehouseService = warehouseService;
        this.offerService = offerService;
    }
    async createSubscription(req) {
        const getWarehouseDetails = await this.warehouseService.getWarehouse(req.warehouse);
        const offer = await this.offerService.getOffer(req.offer);
        let lastActiveDate;
        let currentSubscriptionStartDate;
        let currentSubscriptionEndDate;
        if (getWarehouseDetails.length > 0) {
            if (getWarehouseDetails[0].isActive) {
                currentSubscriptionStartDate = new Date(getWarehouseDetails[0].subscriptionEndDate);
                const tempEndDate = new Date(getWarehouseDetails[0].subscriptionEndDate);
                currentSubscriptionEndDate = new Date(tempEndDate.setDate(tempEndDate.getDate() + offer[0].period));
                const tempLastActiveDate = new Date(getWarehouseDetails[0].subscriptionEndDate);
                lastActiveDate = new Date(tempLastActiveDate.setDate(tempLastActiveDate.getDate() + offer[0].period));
            }
            else {
                currentSubscriptionStartDate = new Date();
                currentSubscriptionEndDate = new Date(new Date().setDate(new Date().getDate() + parseInt(offer[0].period, 10)));
                lastActiveDate = new Date(new Date().setDate(new Date().getDate() + parseInt(offer[0].period, 10)));
            }
            const subscriptionObj = {
                warehouse: req.warehouse,
                offer: offer[0]._id,
                paymentMode: req.paymentMode,
                isActive: true,
                startDate: currentSubscriptionStartDate,
                endDate: currentSubscriptionEndDate
            };
            const subscription = await this.subscriptionService.createSubscription(subscriptionObj);
            const temp = getWarehouseDetails[0].subscription;
            temp.push({ subscription: subscription._id });
            console.log('temp', temp);
            if (subscription) {
                const updateObject = {
                    _id: getWarehouseDetails[0]._id,
                    isActive: true,
                    subscription: temp,
                    subscriptionEndDate: lastActiveDate
                };
                const updateWarehouse = await this.warehouseService.updateWarehouse(updateObject);
                if (updateWarehouse) {
                    return {
                        message: 'Subscription created Successfully',
                        data: JSON.parse(JSON.stringify(subscription))
                    };
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to create the subscription for you. Please try again later');
                }
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to create the subscription for you. Please try again later');
            }
        }
        else {
            throw new routing_controllers_1.NotFoundError('Warehouse details not found');
        }
    }
    async getSubscription(id, wId, sts) {
        const getSubscription = await this.subscriptionService.getSubscription(id, wId, sts);
        if (getSubscription) {
            if (getSubscription.length > 0) {
                return {
                    message: 'Subscription fetch successfully',
                    data: JSON.parse(JSON.stringify(getSubscription))
                };
            }
            else {
                throw new routing_controllers_1.NotFoundError('Subscription not found');
            }
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Subscription]),
    __metadata("design:returntype", Promise)
], SubscriptionController.prototype, "createSubscription", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('subscriptionId')), __param(1, routing_controllers_1.QueryParam('warehouseId')), __param(2, routing_controllers_1.QueryParam('status')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Boolean]),
    __metadata("design:returntype", Promise)
], SubscriptionController.prototype, "getSubscription", null);
SubscriptionController = __decorate([
    routing_controllers_1.JsonController('/subscription'),
    __metadata("design:paramtypes", [subscription_service_1.SubscriptionService,
        warehouse_service_1.WarehouseService,
        offer_service_1.OfferService])
], SubscriptionController);
exports.SubscriptionController = SubscriptionController;
//# sourceMappingURL=subscription.controller.js.map