"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionController = void 0;
const routing_controllers_1 = require("routing-controllers");
const validation_interface_1 = require("../interface/validation.interface");
const transaction_service_1 = require("../services/transaction.service");
const product_service_1 = require("../services/product.service");
let TransactionController = class TransactionController {
    constructor(transactionService, productService) {
        this.transactionService = transactionService;
        this.productService = productService;
    }
    async createTransaction(req) {
        if (req.actionType.toLowerCase() === 'export') {
            let count = 0;
            let qty = 0;
            let productName = '';
            for (const element of req.productDetails) {
                const storage = await this.productService.checkStorageAvailability(element.storageId);
                qty = storage.quantity;
                if (!(storage.quantity >= element.quantity)) {
                    count++;
                    productName = element.name;
                }
            }
            if (count > 0) {
                throw new routing_controllers_1.UnauthorizedError('We have only ' + qty + ' storage package for ' + productName);
            }
            else {
                const transaction = await this.transactionService.createTransaction(req);
                if (transaction) {
                    this.transactionService.managePostTransaction(transaction._id, req);
                    this.productService.manageStorageAfterImpExp(req.productDetails, req.actionType);
                    return transaction;
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to save the bill. Please try again later');
                }
            }
        }
        else {
            let addFaultyEntry;
            if (req.isFaulty) {
                addFaultyEntry = await this.transactionService.createFaultyProduct(req);
            }
            else {
                addFaultyEntry = {
                    _id: undefined
                };
            }
            if (addFaultyEntry) {
                let transaction;
                if (addFaultyEntry._id) {
                    transaction = await this.transactionService.createTransaction(req, addFaultyEntry._id);
                }
                else {
                    transaction = await this.transactionService.createTransaction(req);
                }
                if (transaction) {
                    this.transactionService.managePostTransaction(transaction._id, req);
                    this.productService.manageStorageAfterImpExp(req.productDetails, req.actionType);
                    return transaction;
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to save the bill. Please try again later');
                }
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to save the bill. Please try again later');
            }
        }
    }
    async getTransaction(tId, wId, vId, pId, pagination) {
        const transaction = await this.transactionService.getTransaction(tId, wId, vId, pId, pagination, undefined);
        if (transaction) {
            if (transaction) {
                return transaction;
            }
            else {
                throw new routing_controllers_1.NotFoundError('Transaction details not found');
            }
        }
    }
    async getImport(tId, wId, vId, pId, pagination) {
        const transaction = await this.transactionService.getTransaction(tId, wId, vId, pId, pagination, 'import');
        if (transaction) {
            if (transaction) {
                return transaction;
            }
            else {
                throw new routing_controllers_1.NotFoundError('Transaction details not found');
            }
        }
    }
    async getExport(tId, wId, vId, pId, pagination) {
        const transaction = await this.transactionService.getTransaction(tId, wId, vId, pId, pagination, 'export');
        if (transaction) {
            if (transaction) {
                return transaction;
            }
            else {
                throw new routing_controllers_1.NotFoundError('Transaction details not found');
            }
        }
    }
    async deleteTransaction(req) {
        if (req.actionType.toLowerCase() === 'import') {
            const checkImpSts = await this.transactionService.checkImportStatus(req._id);
            if (!checkImpSts) {
                const deleteImportTransaction = await this.transactionService.deleteImportTransaction(req);
                if (deleteImportTransaction) {
                    return 'Transaction deleted successfully';
                }
                else {
                    throw new routing_controllers_1.UnauthorizedError('You cannot delete this entry as already some products are exported');
                }
            }
            else {
                throw new routing_controllers_1.UnauthorizedError('You cannot delete this entry as already some products are exported');
            }
        }
        else {
            const deleteExport = await this.transactionService.deleteExportTransaction(req);
            if (deleteExport) {
                return 'Transaction deleted successfully';
            }
            else {
                throw new routing_controllers_1.UnauthorizedError('Not able to delete this entry. Please try again later or contact admin');
            }
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Transaction]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "createTransaction", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('transactionId')), __param(1, routing_controllers_1.QueryParam('warehouseId')),
    __param(2, routing_controllers_1.QueryParam('vendorId')), __param(3, routing_controllers_1.QueryParam('productId')), __param(4, routing_controllers_1.QueryParam('paginationObject')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getTransaction", null);
__decorate([
    routing_controllers_1.Get('/get-import'),
    __param(0, routing_controllers_1.QueryParam('transactionId')), __param(1, routing_controllers_1.QueryParam('warehouseId')), __param(2, routing_controllers_1.QueryParam('vendorId')),
    __param(3, routing_controllers_1.QueryParam('productId')), __param(4, routing_controllers_1.QueryParam('paginationObject')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getImport", null);
__decorate([
    routing_controllers_1.Get('/get-export'),
    __param(0, routing_controllers_1.QueryParam('transactionId')), __param(1, routing_controllers_1.QueryParam('warehouseId')), __param(2, routing_controllers_1.QueryParam('vendorId')),
    __param(3, routing_controllers_1.QueryParam('productId')), __param(4, routing_controllers_1.QueryParam('paginationObject')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getExport", null);
__decorate([
    routing_controllers_1.Put('/delete-transaction'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "deleteTransaction", null);
TransactionController = __decorate([
    routing_controllers_1.JsonController('/transaction'),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService,
        product_service_1.ProductService])
], TransactionController);
exports.TransactionController = TransactionController;
//# sourceMappingURL=transaction.controller.js.map