"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductController = void 0;
const routing_controllers_1 = require("routing-controllers");
const validation_interface_1 = require("../interface/validation.interface");
const product_service_1 = require("../services/product.service");
const transaction_service_1 = require("../services/transaction.service");
let ProductController = class ProductController {
    constructor(productService, transactionService) {
        this.productService = productService;
        this.transactionService = transactionService;
    }
    async createProduct(req) {
        const isProductExist = await this.productService.checkProductExist(req);
        if (!isProductExist) {
            const createStorage = await this.productService.createStorage(0);
            if (createStorage) {
                const product = await this.productService.createProduct(req, createStorage._id);
                console.log('product --->', product);
                if (product) {
                    this.transactionService.createMonthBill(req, product._id);
                    return product;
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to save product try again later');
                }
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to save product try again later');
            }
        }
        else {
            throw new routing_controllers_1.UnauthorizedError('Product already exist');
        }
    }
    async getProduct(pid, wId, typeId, vId, pagination) {
        const product = await this.productService.getProduct(pid, wId, typeId, vId, pagination);
        if (product) {
            if (product) {
                return product;
            }
            else {
                throw new routing_controllers_1.NotFoundError('Product not found');
            }
        }
    }
    async updateProduct(req) {
        const product = await this.productService.updateProduct(req);
        if (product) {
            return {
                message: 'Product updated Successfully',
                data: JSON.parse(JSON.stringify(product))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('Product not found');
        }
    }
    async deleteProduct(req) {
        const product = await this.productService.deleteProduct(req);
        if (product) {
            return {
                message: 'Product deleted Successfully',
                data: JSON.parse(JSON.stringify(product))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('Product not found');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Product]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "createProduct", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('productId')), __param(1, routing_controllers_1.QueryParam('warehouseId')), __param(2, routing_controllers_1.QueryParam('packageTypeId')),
    __param(3, routing_controllers_1.QueryParam('vendorId')), __param(4, routing_controllers_1.QueryParam('paginationObject')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "getProduct", null);
__decorate([
    routing_controllers_1.Put('/update'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Product]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "updateProduct", null);
__decorate([
    routing_controllers_1.Put('/delete'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "deleteProduct", null);
ProductController = __decorate([
    routing_controllers_1.JsonController('/product'),
    routing_controllers_1.Authorized(),
    __metadata("design:paramtypes", [product_service_1.ProductService,
        transaction_service_1.TransactionService])
], ProductController);
exports.ProductController = ProductController;
//# sourceMappingURL=product.controller.js.map