"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OfferController = void 0;
const routing_controllers_1 = require("routing-controllers");
const validation_interface_1 = require("../interface/validation.interface");
const offer_service_1 = require("../services/offer.service");
let OfferController = class OfferController {
    constructor(offerService) {
        this.offerService = offerService;
    }
    async createOffer(req) {
        const isOfferExist = await this.offerService.checkOfferExist(req.name);
        if (isOfferExist) {
            throw new routing_controllers_1.UnauthorizedError('Offer name already exist');
        }
        else {
            const offer = await this.offerService.createOffer(req);
            if (offer) {
                return {
                    message: 'Offer created Successfully',
                    data: JSON.parse(JSON.stringify(offer))
                };
            }
            else {
                throw new routing_controllers_1.NotFoundError('offer not found');
            }
        }
    }
    async getOffer(id) {
        const getOffer = await this.offerService.getOffer(id);
        if (getOffer) {
            if (getOffer.length > 0) {
                return {
                    message: 'Offer fetch successfully',
                    data: JSON.parse(JSON.stringify(getOffer))
                };
            }
            else {
                throw new routing_controllers_1.NotFoundError('Offer not found');
            }
        }
    }
    async updateOffer(req) {
        console.log('in update controller');
        const offer = await this.offerService.updateOffer(req);
        if (offer) {
            return {
                message: 'Offer updated Successfully',
                data: JSON.parse(JSON.stringify(offer))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('Offer not found');
        }
    }
    async deleteOffer(req) {
        const offer = await this.offerService.deleteOffer(req);
        if (offer) {
            return {
                message: 'Offer deleted Successfully',
                data: JSON.parse(JSON.stringify(offer))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('offer not found');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Offer]),
    __metadata("design:returntype", Promise)
], OfferController.prototype, "createOffer", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('offerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OfferController.prototype, "getOffer", null);
__decorate([
    routing_controllers_1.Put('/update'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Offer]),
    __metadata("design:returntype", Promise)
], OfferController.prototype, "updateOffer", null);
__decorate([
    routing_controllers_1.Put('/delete'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OfferController.prototype, "deleteOffer", null);
OfferController = __decorate([
    routing_controllers_1.JsonController('/offer'),
    routing_controllers_1.Authorized(),
    __metadata("design:paramtypes", [offer_service_1.OfferService])
], OfferController);
exports.OfferController = OfferController;
//# sourceMappingURL=offer.controller.js.map