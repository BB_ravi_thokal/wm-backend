"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorController = void 0;
const routing_controllers_1 = require("routing-controllers");
const shared_service_1 = require("../services/shared.service");
const utils_1 = require("../helpers/utils");
const auth_service_1 = require("../services/auth.service");
const role_service_1 = require("../services/role.service");
const validation_interface_1 = require("../interface/validation.interface");
const generator = require("generate-password");
const vendor_service_1 = require("../services/vendor.service");
const warehouse_service_1 = require("../services/warehouse.service");
let VendorController = class VendorController {
    constructor(vendorService, warehouseService, sharedService, authService, roleService) {
        this.vendorService = vendorService;
        this.warehouseService = warehouseService;
        this.sharedService = sharedService;
        this.authService = authService;
        this.roleService = roleService;
    }
    async createVendor(req) {
        const isUserExist = await this.sharedService.checkUserExist(req.emailId);
        if (isUserExist) {
            throw new routing_controllers_1.UnauthorizedError('Email Id already exist');
        }
        else {
            const roleId = await this.roleService.getRoleIdByRole((req.role).toUpperCase());
            if (roleId) {
                const password = generator.generate({
                    length: 10,
                    numbers: true
                });
                const encryptPassword = await utils_1.Utils.encryptPassword(password);
                const saveAuth = await this.authService.createAuth(req.emailId, roleId, encryptPassword);
                if (saveAuth) {
                    const vendor = await this.vendorService.createVendor(req, saveAuth._id);
                    if (vendor) {
                        const warehouseDetails = await this.warehouseService.getWarehouse(vendor.warehouse);
                        this.vendorService.sendAccountCreationEmail(vendor, warehouseDetails[0]);
                        return vendor;
                    }
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to create vendor due to server error');
                }
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to create vendor due to server error');
            }
        }
    }
    async getVendor(wId, id, pagination) {
        const vendor = await this.vendorService.getVendor(id, wId, undefined, pagination);
        if (vendor) {
            if (vendor) {
                return vendor;
            }
            else {
                throw new routing_controllers_1.NotFoundError('Vendor not found');
            }
        }
    }
    async updateVendor(req) {
        const vendor = await this.vendorService.updateVendor(req);
        if (vendor) {
            return vendor;
        }
        else {
            throw new routing_controllers_1.NotFoundError('Vendor not found');
        }
    }
    async deleteVendor(req) {
        const role = await this.roleService.getRoleIdByRole(('VENDOR').toUpperCase());
        const getVendor = await this.vendorService.getVendor(req._id);
        if (getVendor) {
            const authData = {
                _id: getVendor[0].auth,
                roleId: role,
                isDeleted: true
            };
            const deleteAuth = await this.authService.deleteAuth(authData);
            if (deleteAuth) {
                const vendor = await this.vendorService.deleteVendor(req);
                if (vendor) {
                    return vendor;
                }
                else {
                    throw new routing_controllers_1.NotFoundError('vendor not found');
                }
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to remove Vendor now please try later');
            }
        }
        else {
            throw new routing_controllers_1.NotFoundError('vendor not found');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Vendor]),
    __metadata("design:returntype", Promise)
], VendorController.prototype, "createVendor", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('warehouseId')), __param(1, routing_controllers_1.QueryParam('vendorId')),
    __param(2, routing_controllers_1.QueryParam('paginationObject')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], VendorController.prototype, "getVendor", null);
__decorate([
    routing_controllers_1.Put('/update'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.UpdateVendor]),
    __metadata("design:returntype", Promise)
], VendorController.prototype, "updateVendor", null);
__decorate([
    routing_controllers_1.Put('/delete'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.UpdateVendor]),
    __metadata("design:returntype", Promise)
], VendorController.prototype, "deleteVendor", null);
VendorController = __decorate([
    routing_controllers_1.JsonController('/vendor'),
    routing_controllers_1.Authorized(),
    __metadata("design:paramtypes", [vendor_service_1.VendorService,
        warehouse_service_1.WarehouseService,
        shared_service_1.SharedService,
        auth_service_1.AuthService,
        role_service_1.RoleService])
], VendorController);
exports.VendorController = VendorController;
//# sourceMappingURL=vendor.controller.js.map