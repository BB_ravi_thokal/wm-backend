"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SenEmailController = void 0;
const routing_controllers_1 = require("routing-controllers");
const send_email_service_1 = require("../services/send-email.service");
let SenEmailController = class SenEmailController {
    constructor(sendEmailService) {
        this.sendEmailService = sendEmailService;
    }
    async sendImportExportEmail(formData, body) {
        await this.sendEmailService.sendTransactionEmail(formData, JSON.parse(body.body));
        return 'success';
    }
};
__decorate([
    routing_controllers_1.Post('/transaction'),
    __param(0, routing_controllers_1.UploadedFiles("file")), __param(1, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SenEmailController.prototype, "sendImportExportEmail", null);
SenEmailController = __decorate([
    routing_controllers_1.JsonController('/email'),
    __metadata("design:paramtypes", [send_email_service_1.SendEmailService])
], SenEmailController);
exports.SenEmailController = SenEmailController;
//# sourceMappingURL=send-email.controller.js.map