"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginController = void 0;
const routing_controllers_1 = require("routing-controllers");
const validation_interface_1 = require("../interface/validation.interface");
const auth_service_1 = require("../services/auth.service");
const warehouse_service_1 = require("../services/warehouse.service");
const vendor_service_1 = require("../services/vendor.service");
const vendor_model_1 = require("../models/vendor.model");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
let LoginController = class LoginController {
    constructor(authService, warehouseService, vendorService) {
        this.authService = authService;
        this.warehouseService = warehouseService;
        this.vendorService = vendorService;
    }
    async login(req) {
        const data = await this.authService.login(req);
        if (data) {
            let temp;
            if (data.role === 'WAREHOUSE') {
                temp = await this.warehouseService.getWarehouse(undefined, data.authId);
            }
            else {
                temp = await await vendor_model_1.default.find({ auth: ObjectId(data.authId) }).populate('warehouse');
            }
            return {
                token: data.token,
                warehouseData: temp[0]
            };
        }
        else {
            throw new routing_controllers_1.UnauthorizedError('Failed to login. Please confirm user id and password');
        }
    }
    async forgotPassword(req) {
        const createTempPass = this.authService.createTemporaryPassword(req);
        if (createTempPass) {
            return 'Temporary password sent on your email';
        }
        else {
            throw new routing_controllers_1.NotFoundError('Entered email is not registered with us');
        }
    }
    async changePassword(req) {
        const createTempPass = this.authService.changePassword(req);
        if (createTempPass) {
            return 'Temporary password sent on your email';
        }
        else {
            throw new routing_controllers_1.NotFoundError('Entered email is not registered with us');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.Login]),
    __metadata("design:returntype", Promise)
], LoginController.prototype, "login", null);
__decorate([
    routing_controllers_1.Post('/forgot-password'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.ForgotPassword]),
    __metadata("design:returntype", Promise)
], LoginController.prototype, "forgotPassword", null);
__decorate([
    routing_controllers_1.Post('/change-password'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.ChangePassword]),
    __metadata("design:returntype", Promise)
], LoginController.prototype, "changePassword", null);
LoginController = __decorate([
    routing_controllers_1.JsonController('/login'),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        warehouse_service_1.WarehouseService,
        vendor_service_1.VendorService])
], LoginController);
exports.LoginController = LoginController;
//# sourceMappingURL=login.controller.js.map