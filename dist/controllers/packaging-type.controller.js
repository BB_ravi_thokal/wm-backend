"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackagingTypeController = void 0;
const routing_controllers_1 = require("routing-controllers");
const validation_interface_1 = require("../interface/validation.interface");
const packaging_type_service_1 = require("../services/packaging-type.service");
let PackagingTypeController = class PackagingTypeController {
    constructor(packagingTypeService) {
        this.packagingTypeService = packagingTypeService;
    }
    async createPackagingType(req) {
        const isPackageExist = await this.packagingTypeService.checkPackagingTypeExist(req);
        if (isPackageExist) {
            throw new routing_controllers_1.UnauthorizedError('Packaging type already exist');
        }
        else {
            const packagingType = await this.packagingTypeService.createPackagingType(req);
            if (packagingType) {
                return packagingType;
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to save packaging type try again later');
            }
        }
    }
    async getPackagingTYpe(id, wId) {
        const packagingType = await this.packagingTypeService.getPackagingType(id, wId);
        if (packagingType) {
            if (packagingType) {
                return packagingType;
            }
            else {
                throw new routing_controllers_1.NotFoundError('packagingType not found');
            }
        }
    }
    async updatePackagingType(req) {
        const packagingType = await this.packagingTypeService.updatePackagingType(req);
        if (packagingType) {
            return {
                message: 'PackagingType updated Successfully',
                data: JSON.parse(JSON.stringify(packagingType))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('PackagingType not found');
        }
    }
    async deletePackagingType(req) {
        const packagingType = await this.packagingTypeService.deletePackagingType(req);
        if (packagingType) {
            return {
                message: 'PackagingType deleted Successfully',
                data: JSON.parse(JSON.stringify(packagingType))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('PackagingType not found');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.PackagingType]),
    __metadata("design:returntype", Promise)
], PackagingTypeController.prototype, "createPackagingType", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('typeId')), __param(1, routing_controllers_1.QueryParam('warehouseId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], PackagingTypeController.prototype, "getPackagingTYpe", null);
__decorate([
    routing_controllers_1.Put('/update'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validation_interface_1.PackagingType]),
    __metadata("design:returntype", Promise)
], PackagingTypeController.prototype, "updatePackagingType", null);
__decorate([
    routing_controllers_1.Put('/delete'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PackagingTypeController.prototype, "deletePackagingType", null);
PackagingTypeController = __decorate([
    routing_controllers_1.JsonController('/packagingType'),
    routing_controllers_1.Authorized(),
    __metadata("design:paramtypes", [packaging_type_service_1.PackagingTypeService])
], PackagingTypeController);
exports.PackagingTypeController = PackagingTypeController;
//# sourceMappingURL=packaging-type.controller.js.map