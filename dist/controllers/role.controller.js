"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleController = void 0;
const routing_controllers_1 = require("routing-controllers");
const role_service_1 = require("../services/role.service");
let RoleController = class RoleController {
    constructor(roleService) {
        this.roleService = roleService;
    }
    async createRole(req) {
        const isRoleExist = await this.roleService.checkRolExist(req.role);
        if (isRoleExist) {
            throw new routing_controllers_1.UnauthorizedError('Role already exists');
        }
        else {
            const create = await this.roleService.createRole(req);
            if (create) {
                return 'Role created successfully';
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to create role due to server error');
            }
        }
    }
    async getAllRole(roleId) {
        const role = await this.roleService.getRole(roleId);
        if (role) {
            return {
                message: 'Role fetched successfully',
                data: JSON.parse(JSON.stringify(role))
            };
        }
        else {
            throw new routing_controllers_1.NotFoundError('Role not found');
        }
    }
    async deleteRole(req) {
        const role = await this.roleService.deleteRole(req.roleId);
        if (role) {
            return "Role deleted successfully";
        }
        else {
            throw new routing_controllers_1.NotFoundError('Role not found');
        }
    }
};
__decorate([
    routing_controllers_1.Post('/create'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "createRole", null);
__decorate([
    routing_controllers_1.Get('/'),
    __param(0, routing_controllers_1.QueryParam('roleId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "getAllRole", null);
__decorate([
    routing_controllers_1.Post('/delete'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "deleteRole", null);
RoleController = __decorate([
    routing_controllers_1.JsonController('/role'),
    routing_controllers_1.Authorized(),
    __metadata("design:paramtypes", [role_service_1.RoleService])
], RoleController);
exports.RoleController = RoleController;
//# sourceMappingURL=role.controller.js.map