"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionService = void 0;
const subscriptionDetails_model_1 = require("../models/subscriptionDetails.model");
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
class SubscriptionService {
    async createSubscription(data) {
        const subscription = new subscriptionDetails_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouse,
            offer: data.offer,
            paymentMode: data.paymentMode,
            isActive: data.isActive,
            startDate: data.startDate,
            endDate: data.endDate
        });
        const subRes = await subscription.save();
        if (subRes) {
            return subRes;
        }
        else {
            return false;
        }
    }
    async getSubscription(id, wId, sts) {
        if (id) {
            const subscription = await subscriptionDetails_model_1.default.find({ _id: ObjectId(id) });
            return subscription;
        }
        else if (wId) {
            const subscription = await subscriptionDetails_model_1.default.find({ warehouse: ObjectId(wId), isActive: true });
            return subscription;
        }
        else if (sts) {
            const subscription = await subscriptionDetails_model_1.default.find({ isActive: sts });
            return subscription;
        }
        else if (!sts) {
            const subscription = await subscriptionDetails_model_1.default.find({ isActive: !sts });
            return subscription;
        }
        else {
            const subscription = await subscriptionDetails_model_1.default.find({});
            return subscription;
        }
    }
    static async deActivateSubscription() {
        const subscription = await subscriptionDetails_model_1.default.find({ isActive: true });
        for (const element of subscription) {
            if (new Date(element.endDate) < new Date()) {
                await subscriptionDetails_model_1.default.updateOne({ _id: ObjectId(element._id) }, { isActive: false });
            }
        }
    }
}
exports.SubscriptionService = SubscriptionService;
//# sourceMappingURL=subscription.service.js.map