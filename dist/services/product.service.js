"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductService = void 0;
const product_model_1 = require("../models/product.model");
const storage_model_1 = require("../models/storage.model");
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
class ProductService {
    async createProduct(data, storageId) {
        const product = new product_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouseId,
            name: data.name,
            packagingType: data.packagingTypeId,
            storage: storageId,
            rate: data.rate,
            vendor: data.vendor
        });
        return await product.save().then(res => {
            return res.populate('storage').populate('packagingType').populate('vendor');
        })
            .catch(err => {
            return false;
        });
    }
    async checkProductExist(productData) {
        const data = await product_model_1.default.findOne({ name: { '$regex': productData.name, $options: 'i' }, warehouse: ObjectId(productData.warehouseId), vendor: ObjectId(productData.vendor), isDeleted: false });
        if (data) {
            return true;
        }
        else {
            return false;
        }
    }
    async getProduct(id, wId, typeId, vId, paginationObj) {
        const query = {
            isDeleted: false,
        };
        if (id) {
            query._id = id;
        }
        else if (typeId && wId && vId) {
            query.packagingType = typeId;
            query.warehouse = wId;
            query.vendor = vId;
        }
        else if (typeId && wId) {
            query.packagingType = typeId;
            query.warehouse = wId;
        }
        else if (wId && vId) {
            query.warehouse = wId;
            query.vendor = vId;
        }
        else if (wId) {
            query.warehouse = wId;
        }
        else if (typeId) {
            query.packagingType = typeId;
        }
        const allProduct = await product_model_1.default.find(query)
            .countDocuments();
        let product;
        if (paginationObj !== '' && paginationObj !== undefined) {
            paginationObj = JSON.parse(paginationObj);
            product = await product_model_1.default.find(query)
                .populate('storage').populate('packagingType').populate('vendor')
                .sort({ createdAt: 'desc' })
                .skip(paginationObj.itemsPerPage * (paginationObj.currentPage - 1))
                .limit(paginationObj.itemsPerPage);
            return { result: product, count: allProduct };
        }
        else {
            product = await product_model_1.default.find(query)
                .populate('storage').populate('packagingType').populate('vendor')
                .sort({ createdAt: 'desc' });
            return { result: product, count: allProduct };
        }
    }
    async updateProduct(data) {
        if (data._id) {
            const product = await product_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (product.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async deleteProduct(data) {
        if (data._id) {
            const product = await product_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (product.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async createStorage(qty) {
        const storage = new storage_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            quantity: qty
        });
        return await storage.save().then(res => {
            return res;
        })
            .catch(err => {
            return false;
        });
    }
    async updateStorage(data) {
        if (data._id) {
            const storage = await storage_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
        }
    }
    async checkStorageAvailability(prodId) {
        const getStorage = await storage_model_1.default.findOne({ _id: ObjectId(prodId) });
        if (getStorage) {
            return getStorage;
        }
    }
    async manageStorageAfterImpExp(prodDetails, actionType) {
        for (const product of prodDetails) {
            const getStorage = await storage_model_1.default.findOne({ _id: ObjectId(product.storageId) });
            if (getStorage) {
                let updateObj;
                if (actionType.toLowerCase() === 'import') {
                    updateObj = {
                        _id: product.storageId,
                        quantity: (parseFloat(getStorage.quantity) + parseFloat(product.quantity))
                    };
                }
                else {
                    updateObj = {
                        _id: product.storageId,
                        quantity: (parseFloat(getStorage.quantity) - parseFloat(product.quantity))
                    };
                }
                this.updateStorage(updateObj);
            }
        }
    }
}
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map