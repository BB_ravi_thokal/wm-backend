"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const auth_model_1 = require("../models/auth.model");
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const utils_1 = require("../helpers/utils");
const routing_controllers_1 = require("routing-controllers");
const EmailService_1 = require("../helpers/EmailService");
const { ObjectId } = mongoose_1.Types;
class AuthService {
    async createAuth(emailId, role, pass) {
        const auth = new auth_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            roleId: role,
            userId: emailId,
            password: pass
        });
        const authRes = await auth.save();
        if (authRes) {
            return authRes;
        }
        else {
            return false;
        }
    }
    async deleteAuth(data) {
        if (data._id) {
            const auth = await auth_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (auth.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async getAuthByUserId(id) {
        if (id) {
            const auth = await auth_model_1.default.findOne({ userId: id, isDeleted: false });
            if (auth) {
                return auth;
            }
            else {
                return false;
            }
        }
    }
    async login(data) {
        const getAuth = await auth_model_1.default.findOne({ userId: data.userId, isDeleted: false }).populate('roleId');
        if (getAuth) {
            const checkPassword = await utils_1.Utils.compareDecryptedKey(data.password, getAuth.password);
            if (checkPassword) {
                const payload = {
                    role: getAuth.roleId.role,
                    userId: data.userId
                };
                const getToken = utils_1.Utils.createJWTToken(payload);
                return {
                    authId: getAuth._id,
                    token: getToken,
                    role: getAuth.roleId.role
                };
            }
            else {
                throw new routing_controllers_1.UnauthorizedError('Please enter correct password');
            }
        }
        else {
            throw new routing_controllers_1.NotFoundError('The mentioned user id does not exist');
        }
    }
    async createTemporaryPassword(data) {
        const checkUser = await this.getAuthByUserId(data.emailId);
        if (checkUser) {
            const random = Math.floor(Math.random() * Math.floor(999999));
            const encryptPassword = await utils_1.Utils.encryptPassword(random.toString());
            const auth = await auth_model_1.default.updateOne({ _id: ObjectId(checkUser._id) }, { temporaryPassword: encryptPassword });
            if (auth.nModified > 0) {
                const mailBody = {
                    to: data.emailId,
                    subject: 'Transaction successful',
                    html: '<div><p>Greetings from Warehouse Management system.</p></br><p> Please use temporary password:'
                        + random +
                        ' for login in system.</p></br><p>Please change the password immediately for security reasons</p></div></br><a href="http://3.135.198.16/dw-home">Warehouse Management System Website</a>',
                };
                const mailStatus = await new EmailService_1.EmailServer().sendEmail(mailBody);
                return true;
            }
            else {
                throw new routing_controllers_1.InternalServerError('Not able to create temporary password for you. Please try again later');
            }
        }
        else {
            return false;
        }
    }
    async changePassword(data) {
        const checkUser = await this.getAuthByUserId(data.emailId);
        if (checkUser) {
            const checkPassword = await utils_1.Utils.compareDecryptedKey(data.temporaryPassword, checkUser.temporaryPassword);
            if (checkPassword) {
                const encryptPassword = await utils_1.Utils.encryptPassword(data.newPassword);
                const auth = await auth_model_1.default.updateOne({ _id: ObjectId(checkUser._id) }, { temporaryPassword: '', password: encryptPassword });
                if (auth.nModified > 0) {
                    return true;
                }
                else {
                    throw new routing_controllers_1.InternalServerError('Not able to change the password. Please try again later');
                }
            }
            else {
                throw new routing_controllers_1.UnauthorizedError('Entered temporary password is wrong');
            }
        }
        else {
            return false;
        }
    }
}
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map