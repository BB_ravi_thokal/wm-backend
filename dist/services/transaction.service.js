"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionService = void 0;
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const transaction_model_1 = require("../models/transaction.model");
const imports_model_1 = require("../models/imports.model");
const faultyProduct_model_1 = require("../models/faultyProduct.model");
const monthBill_model_1 = require("../models/monthBill.model");
const product_model_1 = require("../models/product.model");
const product_service_1 = require("./product.service");
const storage_model_1 = require("../models/storage.model");
const { ObjectId } = mongoose_1.Types;
class TransactionService {
    async createTransaction(data, faultyId = '') {
        const highestUniqueBillId = await this.getBillId(data.warehouse, data.actionType);
        let transaction;
        if (!data.isFaulty) {
            transaction = new transaction_model_1.default({
                _id: new mongoose.Types.ObjectId(),
                warehouse: data.warehouse,
                vendor: data.vendor,
                productDetails: data.productDetails,
                DONumber: data.DONumber,
                actionType: data.actionType,
                vehicleNumber: data.vehicleNumber,
                messageBy: data.messageBy,
                driverContactNumber: data.driverContactNumber,
                isFaulty: data.isFaulty,
                uniqueBillId: highestUniqueBillId,
                createdAt: new Date(data.createdAt),
            });
        }
        else {
            transaction = new transaction_model_1.default({
                _id: new mongoose.Types.ObjectId(),
                warehouse: data.warehouse,
                vendor: data.vendor,
                productDetails: data.productDetails,
                DONumber: data.DONumber,
                actionType: data.actionType,
                vehicleNumber: data.vehicleNumber,
                messageBy: data.messageBy,
                driverContactNumber: data.driverContactNumber,
                isFaulty: data.isFaulty,
                faultyProduct: faultyId,
                uniqueBillId: highestUniqueBillId,
                createdAt: new Date(data.createdAt),
            });
        }
        const saveTransaction = await transaction.save();
        if (saveTransaction) {
            const transactionResult = await transaction_model_1.default.findById({ _id: ObjectId(saveTransaction._id) })
                .populate('warehouse').populate('vendor').populate('faultyProduct');
            return transactionResult;
        }
        else {
            return false;
        }
    }
    async getBillId(wId, actionType) {
        const d = new Date().getDate();
        const m = new Date().getMonth();
        const y = new Date().getFullYear();
        const date = new Date(y, m, 1);
        const highestRecord = await transaction_model_1.default.findOne({ warehouse: ObjectId(wId), actionType, createdAt: { "$gte": new Date(date) } })
            .sort({ uniqueBillId: 'desc' });
        if (highestRecord) {
            if (highestRecord.uniqueBillId) {
                return highestRecord.uniqueBillId + 1;
            }
            else {
                return 1;
            }
        }
        else {
            return 1;
        }
    }
    async getTransaction(id, wId, vId, pId, paginationObj, actionType) {
        const query = {
            isDeleted: false,
        };
        if (actionType) {
            query.actionType = actionType;
        }
        if (paginationObj.startDate && paginationObj.endDate) {
            query.createdAt = { "$gte": new Date(paginationObj.startDate), "$lte": new Date(paginationObj.endDate) };
        }
        if (id) {
            query._id = ObjectId(id);
        }
        else if (wId && vId && pId) {
            query.warehouse = ObjectId(wId);
            query.vendor = ObjectId(vId);
            query.productDetails = { $elemMatch: { _id: pId } };
        }
        else if (wId && vId) {
            query.warehouse = ObjectId(wId);
            query.vendor = ObjectId(vId);
        }
        else if (wId && pId) {
            query.warehouse = ObjectId(wId);
            query.productDetails = { $elemMatch: { _id: pId } };
        }
        else if (wId) {
            query.warehouse = ObjectId(wId);
        }
        else if (vId) {
            query.vendor = ObjectId(vId);
        }
        if (paginationObj.getAllData) {
            const totalCount = await transaction_model_1.default.find(query)
                .countDocuments();
            const transaction = await transaction_model_1.default.find(query)
                .populate('warehouse').populate('vendor').populate('faultyProduct')
                .sort({ createdAt: 'asc' });
            return { result: transaction, count: totalCount };
        }
        else {
            const totalCount = await transaction_model_1.default.find(query)
                .countDocuments();
            const transaction = await transaction_model_1.default.find(query)
                .populate('warehouse').populate('vendor').populate('faultyProduct')
                .sort({ createdAt: 'desc' })
                .skip(paginationObj.itemsPerPage * (paginationObj.currentPage - 1))
                .limit(paginationObj.itemsPerPage);
            return { result: transaction, count: totalCount };
        }
    }
    async createImport(data, tranId, productId, impQty) {
        const importData = new imports_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouse,
            vendor: data.vendor,
            transaction: tranId,
            product: productId,
            importQty: impQty,
            exportQty: 0,
            pendingQty: impQty,
            status: 'Pending',
            DONumber: data.DONumber,
            importDate: new Date(),
            expiryDate: new Date(new Date().setDate(new Date().getDate() + 30)),
        });
        const saveImport = await importData.save();
    }
    async updateImport(data, prodId, qty) {
        const getImportDetails = await imports_model_1.default.find({ product: ObjectId(prodId), status: { $ne: 'Completed' }, isDeleted: false }).sort({ expiryDate: 'ascending' });
        let temp = qty;
        let qtyUsed;
        const importList = [];
        for (const element of getImportDetails) {
            if (temp > 0) {
                if (parseFloat(element.pendingQty) < temp) {
                    temp = temp - parseFloat(element.pendingQty);
                    qtyUsed = parseFloat(element.pendingQty);
                }
                else {
                    qtyUsed = temp;
                    temp = 0;
                }
                const exp = parseFloat(element.exportQty) + qtyUsed;
                const pending = parseFloat(element.importQty) - exp;
                const sts = (pending > 0 ? 'InProcess' : 'Completed');
                const updateObj = {
                    _id: element._id,
                    exportQty: exp,
                    pendingQty: pending,
                    status: sts
                };
                await imports_model_1.default.updateOne({ _id: element._id }, updateObj);
                importList.push({ importId: element._id, quantity: qtyUsed });
            }
            else {
                break;
            }
        }
        return importList;
    }
    async createFaultyProduct(data) {
        const faultyProduct = new faultyProduct_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouse,
            vendor: data.vendor,
            productDetails: data.faultyProduct,
        });
        const saveFaulty = await faultyProduct.save();
        if (saveFaulty) {
            return saveFaulty;
        }
        else {
            return false;
        }
    }
    async managePostTransaction(tranId, data) {
        if ((data.actionType).toLowerCase() === 'import') {
            for (let i = 0; i < data.productDetails.length; i++) {
                await this.createImport(data, tranId, data.productDetails[i]._id, data.productDetails[i].quantity);
                await this.updateMonthBill(data, data.productDetails[i]._id, parseFloat(data.productDetails[i].quantity));
            }
        }
        else {
            const importList = [];
            for (let i = 0; i < data.productDetails.length; i++) {
                console.warn(' 1st in side loop', data.productDetails[i]);
                const temp = await this.updateImport(data, data.productDetails[i]._id, parseFloat(data.productDetails[i].quantity));
                importList.push(...temp);
            }
            this.updateTransaction(tranId, importList);
        }
    }
    async updateMonthBill(data, prodId, qty) {
        const getRate = await product_model_1.default.findOne({ _id: ObjectId(prodId), vendor: ObjectId(data.vendor), warehouse: ObjectId(data.warehouse) });
        const getMonthBill = await monthBill_model_1.default.findOne({ product: ObjectId(prodId), vendor: ObjectId(data.vendor), warehouse: ObjectId(data.warehouse) });
        const updateObj = {
            _id: getMonthBill._id,
            quantity: (getMonthBill.quantity) + qty,
            amount: ((getMonthBill.quantity + qty) * parseFloat(getRate.rate))
        };
        const test = await monthBill_model_1.default.updateOne({ _id: getMonthBill._id }, updateObj);
    }
    async createMonthBill(data, prodId) {
        const monthBill = new monthBill_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouseId,
            vendor: data.vendor,
            product: prodId,
            quantity: 0,
            amount: 0,
        });
        const saveMonthBill = await monthBill.save();
    }
    async updateTransaction(transId, data) {
        const updateObj = {
            _id: transId,
            exportedFrom: data
        };
        await transaction_model_1.default.updateOne({ _id: ObjectId(transId) }, updateObj);
    }
    async deleteImportTransaction(data) {
        const updateImport = await imports_model_1.default.updateMany({ transaction: ObjectId(data._id) }, { isDeleted: true });
        const updateTransaction = await transaction_model_1.default.updateOne({ _id: ObjectId(data._id) }, { isDeleted: true });
        this.manageMonthBillOnDeleteImport(data);
        this.manageStorageAfterDeleteTransaction(data, 'import');
        if (updateTransaction.nModified > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    async checkImportStatus(transId) {
        const checkSts = await imports_model_1.default.find({ transaction: transId, status: { $in: ['InProcess', 'Completed'] }, isDeleted: false });
        if (checkSts.length > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    async deleteExportTransaction(data) {
        const updateTransaction = await transaction_model_1.default.updateOne({ _id: ObjectId(data._id) }, { isDeleted: true });
        for (const element of data.exportedFrom) {
            const getImportDetails = await imports_model_1.default.findOne({ _id: ObjectId(element.importId), isDeleted: false });
            const checkExportAvailable = await transaction_model_1.default.find({ exportedFrom: { $elemMatch: { importId: element.importId } }, isDeleted: false });
            const updateObj = {
                _id: getImportDetails._id,
                exportQty: parseFloat(getImportDetails.exportQty) - parseFloat(element.quantity),
                pendingQty: parseFloat(getImportDetails.pendingQty) + parseFloat(element.quantity),
                status: (checkExportAvailable.length > 0) ? 'InProcess' : 'Pending'
            };
            await imports_model_1.default.updateOne({ _id: element.importId }, updateObj);
        }
        if (updateTransaction.nModified > 0) {
            this.manageStorageAfterDeleteTransaction(data, 'export');
            return true;
        }
        else {
            return false;
        }
    }
    async manageStorageAfterDeleteTransaction(data, action) {
        const productService = new product_service_1.ProductService();
        for (const element of data.productDetails) {
            const getStorage = await storage_model_1.default.findOne({ _id: ObjectId(element.storageId) });
            if (getStorage) {
                const updateStorage = {
                    _id: element.storageId,
                    quantity: action === 'export' ? (parseFloat(getStorage.quantity) + parseFloat(element.quantity))
                        : (parseFloat(getStorage.quantity) - parseFloat(element.quantity))
                };
                productService.updateStorage(updateStorage);
            }
        }
    }
    async deleteFromMonthBill(data, prodId, qty) {
        const getRate = await product_model_1.default.findOne({ _id: ObjectId(prodId), vendor: ObjectId(data.vendor._id), warehouse: ObjectId(data.warehouse._id) });
        const getMonthBill = await monthBill_model_1.default.findOne({ product: ObjectId(prodId), vendor: ObjectId(data.vendor._id), warehouse: ObjectId(data.warehouse._id) });
        const updateObj = {
            _id: getMonthBill._id,
            quantity: (getMonthBill.quantity) - qty,
            amount: ((getMonthBill.quantity - qty) * parseFloat(getRate.rate))
        };
        const test = await monthBill_model_1.default.updateOne({ _id: getMonthBill._id }, updateObj);
    }
    async manageMonthBillOnDeleteImport(data) {
        const importArray = await imports_model_1.default.find({ transaction: ObjectId(data._id) });
        for (const element of importArray) {
            await this.deleteFromMonthBill(data, element.product, element.importQty);
        }
    }
}
exports.TransactionService = TransactionService;
//# sourceMappingURL=transaction.service.js.map