"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SharedService = void 0;
const auth_model_1 = require("../models/auth.model");
class SharedService {
    async checkUserExist(email) {
        const data = await auth_model_1.default.findOne({ userId: { '$regex': email, $options: 'i' }, isDeleted: false });
        if (data) {
            return data;
        }
        else {
            return false;
        }
    }
}
exports.SharedService = SharedService;
//# sourceMappingURL=shared.service.js.map