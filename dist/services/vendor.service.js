"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorService = void 0;
const vendor_model_1 = require("../models/vendor.model");
const auth_model_1 = require("../models/auth.model");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
const mongoose = require("mongoose");
const EmailService_1 = require("../helpers/EmailService");
const utils_1 = require("../helpers/utils");
class VendorService {
    async createVendor(data, authId) {
        const vendor = new vendor_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            auth: authId,
            warehouse: data.warehouseId,
            name: data.name,
            emailId: data.emailId,
            contactNumber: data.contactNumber,
            TotalAmountPaid: 0,
            pendingDues: 0,
        });
        return await vendor.save().then(res => {
            if (res) {
                return res;
            }
            else {
                return false;
            }
        });
    }
    async sendAccountCreationEmail(vendorData, warehouseData) {
        const random = Math.floor(Math.random() * Math.floor(999999));
        const encryptPassword = await utils_1.Utils.encryptPassword(random.toString());
        await auth_model_1.default.updateOne({ _id: ObjectId(vendorData.auth) }, { temporaryPassword: encryptPassword });
        const mailBody = {
            to: vendorData.emailId,
            cc: warehouseData.ownerEmailId,
            subject: 'New account created',
            html: '<div><p>Welcome to Warehouse Management system.</p></br><p>' + warehouseData.name + ' has added you as a vendor.</p></br><p> Please use temporary password:'
                + random +
                ' for login in system</p></div></br><a href="http://3.135.198.16/dw-home">Warehouse Management System Website</a>'
        };
        await new EmailService_1.EmailServer().sendEmail(mailBody);
    }
    async getVendor(id, warehouseId, authId, paginationObj) {
        const query = {
            isDeleted: false
        };
        if (id) {
            query._id = id;
        }
        else if (warehouseId) {
            query.warehouse = warehouseId;
        }
        else if (authId) {
            query.auth = authId;
        }
        const allVendors = await vendor_model_1.default.find(query).countDocuments();
        let vendors;
        if (paginationObj !== '' && paginationObj !== undefined) {
            vendors = await vendor_model_1.default.find(query).populate('warehouse')
                .sort({ createdAt: 'desc' })
                .skip(paginationObj.itemsPerPage * (paginationObj.currentPage - 1))
                .limit(paginationObj.itemsPerPage);
            return { result: vendors, count: allVendors };
        }
        else {
            vendors = await vendor_model_1.default.find(query).populate('warehouse')
                .sort({ createdAt: 'desc' });
            return { result: vendors, count: allVendors };
        }
    }
    async updateVendor(data) {
        if (data._id) {
            const vendor = await vendor_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (vendor.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async deleteVendor(data) {
        if (data._id) {
            const vendor = await vendor_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (vendor.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
exports.VendorService = VendorService;
//# sourceMappingURL=vendor.service.js.map