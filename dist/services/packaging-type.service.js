"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackagingTypeService = void 0;
const packagingType_model_1 = require("../models/packagingType.model");
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
class PackagingTypeService {
    async createPackagingType(data) {
        const packagingType = new packagingType_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouseId,
            packagingType: data.packagingType
        });
        return await packagingType.save().then(res => {
            return res;
        })
            .catch(err => {
            return false;
        });
    }
    async checkPackagingTypeExist(data) {
        const packageType = await packagingType_model_1.default.findOne({ packagingType: data.packagingType, warehouse: ObjectId(data.warehouseId), isDeleted: false });
        if (packageType) {
            return true;
        }
        else {
            return false;
        }
    }
    async getPackagingType(id, wId) {
        if (id) {
            const packagingType = await packagingType_model_1.default.find({ _id: ObjectId(id), isDeleted: false }).sort({ createdAt: 'desc' });
            return packagingType;
        }
        else if (wId) {
            const packagingType = await packagingType_model_1.default.find({ warehouse: wId, isDeleted: false }).sort({ createdAt: 'desc' });
            return packagingType;
        }
        else {
            const packagingType = await packagingType_model_1.default.find({ isDeleted: false }).sort({ createdAt: 'desc' });
            return packagingType;
        }
    }
    async updatePackagingType(data) {
        if (data._id) {
            const packagingType = await packagingType_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (packagingType.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async deletePackagingType(data) {
        if (data._id) {
            const packagingType = await packagingType_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (packagingType.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
exports.PackagingTypeService = PackagingTypeService;
//# sourceMappingURL=packaging-type.service.js.map