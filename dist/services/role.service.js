"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleService = void 0;
const role_model_1 = require("../models/role.model");
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
class RoleService {
    async getRoleIdByRole(roleName) {
        const role = await role_model_1.default.findOne({ role: roleName });
        if (role) {
            return role._id;
        }
        else {
            return false;
        }
    }
    async createRole(data) {
        const role = new role_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            role: data.role
        });
        return await role.save();
    }
    async checkRolExist(roleName) {
        return await role_model_1.default.findOne({ role: roleName });
    }
    async getRole(id) {
        if (id) {
            const role = await role_model_1.default.find({ _id: ObjectId(id) });
            return role;
        }
        else {
            const role = await role_model_1.default.find({});
            return role;
        }
    }
    async deleteRole(id) {
        if (id) {
            const role = await role_model_1.default.deleteOne({ _id: ObjectId(id) });
            return role;
        }
    }
}
exports.RoleService = RoleService;
//# sourceMappingURL=role.service.js.map