"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendEmailService = void 0;
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
const EmailService_1 = require("../helpers/EmailService");
class SendEmailService {
    async sendTransactionEmail(files, body) {
        const attachments = [];
        for (const element of files) {
            const splitArr = element.originalname.split(".");
            const file = splitArr[0].split(" ").join("_");
            const extention = splitArr[splitArr.length - 1];
            const modifiedFileName = `${file}${new Date().getTime()}.${extention}`;
            const temp = {
                filename: modifiedFileName,
                content: element.buffer,
            };
            attachments.push(temp);
        }
        const mailBody = {
            to: body.vendor.emailId,
            cc: body.warehouse.ownerEmailId,
            subject: 'Transaction successful',
            html: body.content,
            attachment: attachments
        };
        const mailStatus = await new EmailService_1.EmailServer().sendEmail(mailBody);
        return true;
    }
}
exports.SendEmailService = SendEmailService;
//# sourceMappingURL=send-email.service.js.map