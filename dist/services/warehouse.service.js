"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WarehouseService = void 0;
const warehouse_model_1 = require("../models/warehouse.model");
const mongoose_1 = require("mongoose");
const { ObjectId } = mongoose_1.Types;
const mongoose = require("mongoose");
class WarehouseService {
    async createWarehouse(data, authId) {
        const warehouse = new warehouse_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            auth: authId,
            name: data.name,
            ownerName: data.ownerName,
            ownerContactNumber: data.ownerContactNumber,
            ownerEmailId: data.ownerEmailId,
            address: data.address
        });
        return await warehouse.save().then(warehouseRes => {
            if (warehouseRes) {
                return warehouseRes;
            }
            else {
                return false;
            }
        });
    }
    async getWarehouse(id, authId) {
        if (id && authId) {
            const warehouse = await warehouse_model_1.default.find({ _id: ObjectId(id), isDeleted: false, auth: ObjectId(authId) });
            return warehouse;
        }
        else if (id) {
            const warehouse = await warehouse_model_1.default.find({ _id: ObjectId(id), isDeleted: false });
            return warehouse;
        }
        else if (authId) {
            const warehouse = await warehouse_model_1.default.find({ auth: ObjectId(authId), isDeleted: false });
            return warehouse;
        }
        else {
            const warehouse = await warehouse_model_1.default.find({ isDeleted: false });
            return warehouse;
        }
    }
    async updateWarehouse(data) {
        if (data._id) {
            const warehouse = await warehouse_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (warehouse.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async deleteWarehouse(data) {
        if (data._id) {
            const warehouse = await warehouse_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (warehouse.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
exports.WarehouseService = WarehouseService;
//# sourceMappingURL=warehouse.service.js.map