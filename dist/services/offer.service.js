"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OfferService = void 0;
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const offer_model_1 = require("../models/offer.model");
const { ObjectId } = mongoose_1.Types;
class OfferService {
    async checkOfferExist(offerName) {
        const isOffer = await offer_model_1.default.findOne({ name: offerName, isDeleted: false });
        if (isOffer) {
            return true;
        }
        else {
            return false;
        }
    }
    async createOffer(data) {
        const offer = new offer_model_1.default({
            _id: new mongoose.Types.ObjectId(),
            name: data.name,
            period: data.period,
            amount: data.amount
        });
        return await offer.save().then(res => {
            return res;
        })
            .catch(err => {
            return false;
        });
    }
    async getOffer(id) {
        if (id) {
            const offer = await offer_model_1.default.find({ _id: ObjectId(id), isDeleted: false });
            return offer;
        }
        else {
            const offer = await offer_model_1.default.find({ isDeleted: false });
            return offer;
        }
    }
    async updateOffer(data) {
        if (data._id) {
            const offer = await offer_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (offer.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    async deleteOffer(data) {
        if (data._id) {
            const offer = await offer_model_1.default.updateOne({ _id: ObjectId(data._id) }, data);
            if (offer.nModified > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
exports.OfferService = OfferService;
//# sourceMappingURL=offer.service.js.map