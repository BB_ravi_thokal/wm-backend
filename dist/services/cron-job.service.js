"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobService = void 0;
const schedule = require("node-schedule");
const warehouse_model_1 = require("../models/warehouse.model");
const vendor_model_1 = require("../models/vendor.model");
const imports_model_1 = require("../models/imports.model");
const monthBill_model_1 = require("../models/monthBill.model");
const product_model_1 = require("../models/product.model");
const mongoose_1 = require("mongoose");
const handlebars = require("handlebars");
const fs = require("fs");
const EmailService_1 = require("../helpers/EmailService");
const jsDom = require("jsdom");
const subscription_service_1 = require("./subscription.service");
const { ObjectId } = mongoose_1.Types;
const document = new jsDom.JSDOM('html').window.document;
class JobService {
    async dailyRunner() {
        const rule = new schedule.RecurrenceRule();
        rule.dayOfWeek = [0, new schedule.Range(0, 6)];
        rule.hour = 1;
        const job = schedule.scheduleJob(rule, async function () {
            console.log('daily runner called', new Date());
            const getImports = await imports_model_1.default.find({ status: { $ne: 'Completed' }, expiryDate: { $lt: new Date() }, isDeleted: false });
            if (getImports.length > 0) {
                for (const element of getImports) {
                    const expDate = element.expiryDate;
                    const updateObjImport = {
                        _id: element._id,
                        expiryDate: new Date(expDate.setDate(new Date(expDate).getDate() + 30)),
                    };
                    await imports_model_1.default.updateOne({ _id: element._id }, updateObjImport);
                    const getMonthBill = await monthBill_model_1.default.findOne({ product: ObjectId(element.product) });
                    const getRate = await product_model_1.default.findOne({ _id: ObjectId(element.product), vendor: ObjectId(element.vendor), warehouse: ObjectId(element.warehouse) });
                    const updateObj = {
                        _id: getMonthBill._id,
                        quantity: getMonthBill.quantity + element.pendingQty,
                        amount: getMonthBill.amount + (getRate.rate * element.pendingQty)
                    };
                    await monthBill_model_1.default.updateOne({ _id: getMonthBill._id }, updateObj);
                }
            }
            subscription_service_1.SubscriptionService.deActivateSubscription();
        });
    }
    async monthlyBillGenerator() {
        const rule = new schedule.RecurrenceRule();
        rule.date = 3;
        rule.hour = 2;
        const billDetails = [];
        const job = schedule.scheduleJob(rule, async function () {
            console.log('Monthly runner called', new Date());
            try {
                const getWarehouse = await warehouse_model_1.default.find({ isActive: true, isDeleted: false });
                const billData = [];
                for (const warehouseObj of getWarehouse) {
                    const getVendor = await vendor_model_1.default.find({ warehouse: ObjectId(warehouseObj._id), isDeleted: false });
                    for (const vendorObj of getVendor) {
                        const getProduct = await product_model_1.default.find({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), isDeleted: false }).populate('packagingType');
                        let productDetails = [];
                        let totalAmount = 0.00;
                        let totalQty = 0;
                        for (const productObj of getProduct) {
                            const getMonthBill = await monthBill_model_1.default.findOne({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), product: ObjectId(productObj._id) });
                            if (getMonthBill.amount > 0) {
                                const tempProd = {
                                    productName: productObj.name,
                                    packagingType: productObj.packagingType.packagingType,
                                    amount: getMonthBill.amount,
                                    quantity: getMonthBill.quantity
                                };
                                totalAmount = totalAmount + tempProd.amount;
                                totalQty = totalQty + tempProd.quantity;
                                productDetails.push(tempProd);
                            }
                        }
                        if (productDetails.length > 0) {
                            const temp = {
                                warehouseName: warehouseObj.name,
                                warehouseOwnerName: warehouseObj.ownerName,
                                ownerContactNumber: warehouseObj.ownerContactNumber,
                                ownerEmailId: warehouseObj.ownerEmailId,
                                vendorName: vendorObj.name,
                                vendorEmail: vendorObj.emailId,
                                vendorContactNumber: vendorObj.contactNumber,
                                products: productDetails,
                                totalAmt: totalAmount,
                                totalQuantity: totalQty
                            };
                            billData.push(temp);
                            productDetails = [];
                        }
                    }
                }
                console.log('billData-------------------->', billData);
                if (billData.length > 0) {
                    this.readHTMLFile(billData);
                }
            }
            catch (err) {
                console.log('error', err);
            }
            await monthBill_model_1.default.updateMany({}, { quantity: 0, amount: 0 });
        }.bind(this));
    }
    async testFunctionDaily() {
        const getImports = await imports_model_1.default.find({ status: { $ne: 'Completed' }, expiryDate: { $lt: new Date() }, isDeleted: false });
        console.log('imports array details', getImports);
        if (getImports.length > 0) {
            console.log('imports array', getImports);
            for (const element of getImports) {
                const expDate = element.expiryDate;
                const updateObjImport = {
                    _id: element._id,
                    expiryDate: new Date(expDate.setDate(new Date(expDate).getDate() + 30)),
                };
                await imports_model_1.default.updateOne({ _id: element._id }, updateObjImport);
                const getMonthBill = await monthBill_model_1.default.findOne({ product: ObjectId(element.product) });
                const getRate = await product_model_1.default.findOne({ _id: ObjectId(element.product), vendor: ObjectId(element.vendor), warehouse: ObjectId(element.warehouse) });
                const updateObj = {
                    _id: getMonthBill._id,
                    quantity: getMonthBill.quantity + element.pendingQty,
                    amount: getMonthBill.amount + (getRate.rate * element.pendingQty)
                };
                await monthBill_model_1.default.updateOne({ _id: getMonthBill._id }, updateObj);
            }
        }
        return getImports;
    }
    async testFunction() {
        try {
            const getWarehouse = await warehouse_model_1.default.find({ isActive: true, isDeleted: false });
            const billData = [];
            for (const warehouseObj of getWarehouse) {
                const getVendor = await vendor_model_1.default.find({ warehouse: ObjectId(warehouseObj._id), isDeleted: false });
                for (const vendorObj of getVendor) {
                    const getProduct = await product_model_1.default.find({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), isDeleted: false }).populate('packagingType');
                    let productDetails = [];
                    let totalAmount = 0.00;
                    let totalQty = 0;
                    for (const productObj of getProduct) {
                        const getMonthBill = await monthBill_model_1.default.findOne({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), product: ObjectId(productObj._id) });
                        if (getMonthBill.amount > 0) {
                            const tempProd = {
                                productName: productObj.name,
                                packagingType: productObj.packagingType.packagingType,
                                amount: getMonthBill.amount,
                                quantity: getMonthBill.quantity
                            };
                            totalAmount = totalAmount + tempProd.amount;
                            totalQty = totalQty + tempProd.quantity;
                            productDetails.push(tempProd);
                        }
                    }
                    if (productDetails.length > 0) {
                        const temp = {
                            warehouseName: warehouseObj.name,
                            warehouseOwnerName: warehouseObj.ownerName,
                            ownerContactNumber: warehouseObj.ownerContactNumber,
                            ownerEmailId: warehouseObj.ownerEmailId,
                            vendorName: vendorObj.name,
                            vendorEmail: vendorObj.emailId,
                            vendorContactNumber: vendorObj.contactNumber,
                            products: productDetails,
                            totalAmt: totalAmount,
                            totalQuantity: totalQty
                        };
                        billData.push(temp);
                        productDetails = [];
                    }
                }
            }
            console.log('billData-------------------->', billData);
            if (billData.length > 0) {
                this.readHTMLFile(billData);
            }
        }
        catch (err) {
            console.log('error', err);
        }
        await monthBill_model_1.default.updateMany({}, { quantity: 0, amount: 0 });
    }
    async readHTMLFile(data) {
        await fs.readFile('./src/html/monthly-bill-report.html', { encoding: 'utf-8' }, async (err, html) => {
            if (err) {
                return err;
            }
            else {
                let count = 0;
                for (const element of data) {
                    count++;
                    console.log('email sent to', count);
                    const template = handlebars.compile(html);
                    const table = document.createElement('table');
                    const tr1 = document.createElement('tr');
                    const th1 = document.createElement('th');
                    const th2 = document.createElement('th');
                    const th3 = document.createElement('th');
                    const th4 = document.createElement('th');
                    const th5 = document.createElement('th');
                    const header1 = document.createTextNode('Product Name');
                    const header2 = document.createTextNode('Packaging Type');
                    const header3 = document.createTextNode('Quantity');
                    const header4 = document.createTextNode('Rate');
                    const header5 = document.createTextNode('Amount');
                    th1.appendChild(header1);
                    th2.appendChild(header2);
                    th3.appendChild(header3);
                    th4.appendChild(header4);
                    th5.appendChild(header5);
                    tr1.appendChild(th1);
                    tr1.appendChild(th2);
                    tr1.appendChild(th3);
                    tr1.appendChild(th4);
                    tr1.appendChild(th5);
                    tr1.style.border = '1px solid';
                    table.appendChild(tr1);
                    let totalCount = 0;
                    for (const item of element.products) {
                        const tr = document.createElement('tr');
                        const td1 = document.createElement('td');
                        const td2 = document.createElement('td');
                        const td3 = document.createElement('td');
                        const td4 = document.createElement('td');
                        const td5 = document.createElement('td');
                        const rate = item.amount / item.quantity;
                        totalCount = totalCount + item.amount;
                        const text1 = document.createTextNode(item.productName);
                        const text2 = document.createTextNode(item.packagingType);
                        const text3 = document.createTextNode(item.quantity);
                        const text4 = document.createTextNode(rate.toFixed(2));
                        const text5 = document.createTextNode(item.amount);
                        td1.appendChild(text1);
                        td2.appendChild(text2);
                        td3.appendChild(text3);
                        td4.appendChild(text4);
                        td5.appendChild(text5);
                        td1.style.padding = '8px';
                        td2.style.padding = '8px';
                        td3.style.padding = '8px';
                        td4.style.padding = '8px';
                        td5.style.padding = '8px';
                        tr.appendChild(td1);
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        tr.appendChild(td4);
                        tr.appendChild(td5);
                        tr.style.border = '1px solid';
                        table.appendChild(tr);
                    }
                    const tr2 = document.createElement('tr');
                    const tLast1 = document.createElement('td');
                    const tLast2 = document.createElement('td');
                    const tLast3 = document.createElement('td');
                    const tLast4 = document.createElement('td');
                    const tLast5 = document.createElement('td');
                    const bottom1 = document.createTextNode('Total');
                    const bottom2 = document.createTextNode('');
                    const bottom3 = document.createTextNode('');
                    const bottom4 = document.createTextNode('');
                    const bottom5 = document.createTextNode(totalCount.toFixed(2));
                    tLast1.appendChild(bottom1);
                    tLast2.appendChild(bottom2);
                    tLast3.appendChild(bottom3);
                    tLast4.appendChild(bottom4);
                    tLast5.appendChild(bottom5);
                    tr2.appendChild(tLast1);
                    tr2.appendChild(tLast2);
                    tr2.appendChild(tLast3);
                    tr2.appendChild(tLast4);
                    tr2.appendChild(tLast5);
                    tr2.style.border = '1px solid';
                    table.appendChild(tr2);
                    table.style.margin = '20px';
                    table.style.width = '100%';
                    table.style.border = '1px solid #314456';
                    const replacements = {
                        DATE: new Date().toLocaleString(),
                        WAREHOUSE_NAME: element.warehouseName,
                        WAREHOUSE_EMAIL: element.ownerEmailId,
                        WAREHOUSE_MOB: element.ownerContactNumber,
                        VENDOR_NAME: element.vendorName,
                        TABLE: table.outerHTML
                    };
                    const htmlToSend = template(replacements);
                    const mailBody = {
                        to: element.vendorEmail,
                        cc: element.ownerEmailId,
                        subject: 'Your last month bill',
                        html: htmlToSend,
                    };
                    const mailStatus = await new EmailService_1.EmailServer().sendEmail(mailBody);
                    console.log('mail status', mailStatus);
                }
                count = 0;
            }
        });
    }
    ;
}
exports.JobService = JobService;
//# sourceMappingURL=cron-job.service.js.map