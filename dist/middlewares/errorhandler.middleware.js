"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomErrorHandler = void 0;
const class_validator_1 = require("class-validator");
const logger_1 = require("../logger");
class CustomErrorHandler {
    error(error, request, response, next) {
        const statusCode = !!error.httpCode ? error.httpCode : response.statusCode;
        let errorObj;
        if (!!error.errors && class_validator_1.isArray(error.errors)) {
            const validationErrors = error.errors.reduce((acc, currentElement) => {
                if (currentElement.children.length > 0 && class_validator_1.isArray(currentElement.children)) {
                    acc.push(...processChildErrors(currentElement.children, currentElement.property));
                }
                else {
                    acc.push({
                        parent_property: null,
                        property: currentElement.property,
                        constraints: Object.values(currentElement.constraints).join(" & ")
                    });
                }
                return acc;
            }, []);
            errorObj = {
                statusCode,
                date: Date.now(),
                message: validationErrors,
                request_url: request.url,
                request_data: request.method === "GET" ? request.query || request.params : request.body,
                trace: error.stack
            };
            return response.status(statusCode).json(validationErrors);
        }
        errorObj = {
            statusCode,
            date: Date.now(),
            message: error.message,
            request_url: request.url,
            request_data: request.method === "GET" ? request.query || request.params : request.body,
            trace: error.stack
        };
        logger_1.logger.error(errorObj);
        const Error = {
            status: false,
            message: error.message,
        };
        response.status(statusCode).json(Error);
    }
}
exports.CustomErrorHandler = CustomErrorHandler;
const processChildErrors = (errArr, parent) => {
    const errorMessageArr = errArr.reduce((acc, current) => {
        if (current.children.length > 0) {
            acc.push(...processChildErrors(current.children, parent));
        }
        else {
            acc.push({
                parent_property: parent,
                property: current.property,
                message: Object.values(current.constraints).join(" & ")
            });
        }
        return acc;
    }, []);
    return errorMessageArr;
};
//# sourceMappingURL=errorhandler.middleware.js.map