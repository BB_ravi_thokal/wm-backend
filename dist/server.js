"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const cron_job_service_1 = require("./services/cron-job.service");
app_1.default.listen(process.env.PORT || 4000, () => {
    console.log('Server started on port 4000 .....');
    const jobService = new cron_job_service_1.JobService();
    jobService.dailyRunner();
    jobService.monthlyBillGenerator();
});
//# sourceMappingURL=server.js.map