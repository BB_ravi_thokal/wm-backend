"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const authSchema = new Schema({
    _id: Schema.Types.ObjectId,
    roleId: { type: Schema.Types.ObjectId, ref: 'Role' },
    userId: String,
    password: String,
    temporaryPassword: { type: String, default: '' },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('Auth', authSchema, 'Auth');
//# sourceMappingURL=auth.model.js.map