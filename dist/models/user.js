"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userSchema = new Schema({
    _Id: mongoose.Schema.Types.ObjectId,
    name: String,
    mobileNumber: String,
    emailId: String,
});
exports.default = mongoose.model('User', userSchema);
//# sourceMappingURL=user.js.map