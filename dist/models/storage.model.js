"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const storageSchema = new Schema({
    _id: Schema.Types.ObjectId,
    quantity: { type: Number, required: true }
}, { timestamps: true });
exports.default = mongoose.model('ProductStorage', storageSchema, 'ProductStorage');
//# sourceMappingURL=storage.model.js.map