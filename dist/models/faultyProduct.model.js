"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const faultyProductSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    productDetails: { type: Object, required: true },
}, { timestamps: true });
exports.default = mongoose.model('FaultyProduct', faultyProductSchema, 'FaultyProduct');
//# sourceMappingURL=faultyProduct.model.js.map