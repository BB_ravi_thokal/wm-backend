"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const offerSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: { type: String, required: true },
    period: { type: Number, required: true },
    amount: { type: Number, required: true },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('Offer', offerSchema, 'Offer');
//# sourceMappingURL=offer.model.js.map