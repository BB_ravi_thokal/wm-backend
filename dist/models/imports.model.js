"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const importSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    transaction: { type: Schema.Types.ObjectId, ref: 'Transaction' },
    product: { type: Schema.Types.ObjectId, ref: 'Product' },
    importQty: { type: Number, required: true },
    exportQty: { type: Number, required: true },
    pendingQty: { type: Number, required: true },
    status: { enum: ['Pending', 'InProcess', 'Completed'] },
    DONumber: String,
    importDate: { type: Date, required: true },
    expiryDate: { type: Date, required: true },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('Import', importSchema, 'Import');
//# sourceMappingURL=imports.model.js.map