"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const roleSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    role: String,
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('Role', roleSchema, 'Role');
//# sourceMappingURL=role.model.js.map