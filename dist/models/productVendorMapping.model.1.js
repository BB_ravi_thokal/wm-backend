"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const productVendorMapping = new Schema({
    _Id: Schema.Types.ObjectId,
    warehouse: [{ type: Schema.Types.ObjectId, ref: 'Warehouse' }],
    product: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
    rate: [{ type: Number, require: true }],
    packagingType: [{ type: String, require: true }]
});
exports.default = mongoose.model('ProductVendorMapping', productVendorMapping, 'ProductVendorMapping');
//# sourceMappingURL=productVendorMapping.model.1.js.map