"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const productVendorMapping = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    product: { type: Schema.Types.ObjectId, ref: 'Product' },
    rate: { type: Number, require: true },
    packagingType: { type: String, require: true },
    storage: { type: Schema.Types.ObjectId, ref: 'ProductStorage' },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('ProductVendorMapping', productVendorMapping, 'ProductVendorMapping');
//# sourceMappingURL=productVendorMapping.model.js.map