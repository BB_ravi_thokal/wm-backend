"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const packagingTypeSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    packagingType: { type: String, require: true },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('PackagingType', packagingTypeSchema, 'PackagingType');
//# sourceMappingURL=packagingType.model.js.map