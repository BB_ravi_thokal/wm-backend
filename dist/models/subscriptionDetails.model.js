"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const subscriptionSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    offer: { type: Schema.Types.ObjectId, ref: 'Offer' },
    paymentMode: { type: String, required: true },
    startDate: { type: Date, required: true },
    endDate: { type: Date, required: true },
    isActive: { type: Boolean, default: true }
}, { timestamps: true });
exports.default = mongoose.model('Subscription', subscriptionSchema, 'Subscription');
//# sourceMappingURL=subscriptionDetails.model.js.map