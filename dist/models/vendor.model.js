"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const vendorSchema = new Schema({
    _id: Schema.Types.ObjectId,
    auth: { type: Schema.Types.ObjectId, ref: 'Auth' },
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    name: { type: String, required: true },
    emailId: { type: String, required: true },
    contactNumber: { type: String, required: true },
    TotalAmountPaid: { type: String },
    pendingDues: { type: String, required: true },
    isDeleted: { type: Boolean, default: false }
}, { timestamps: true });
exports.default = mongoose.model('Vendor', vendorSchema, 'Vendor');
//# sourceMappingURL=vendor.model.js.map