"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const monthlyBillSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    product: { type: Schema.Types.ObjectId, ref: 'Product' },
    quantity: { type: Number, require: true },
    amount: { type: Number, require: true },
}, { timestamps: true });
exports.default = mongoose.model('MonthBill', monthlyBillSchema, 'MonthBill');
//# sourceMappingURL=monthBill.model.js.map