"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const fs_1 = require("fs");
const winston = require("winston");
const logDir = `./logs`;
const fileName = Date.now().toLocaleString;
if (!fs_1.existsSync(logDir)) {
    fs_1.mkdirSync(logDir);
}
exports.logger = winston.createLogger({
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: `${logDir}/${fileName}` }),
    ]
});
//# sourceMappingURL=logger.js.map