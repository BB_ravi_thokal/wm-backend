"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Utils = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
const config_1 = require("../config");
const routing_controllers_1 = require("routing-controllers");
const crypto_ts_1 = require("crypto-ts");
class Utils {
    static createJWTToken(payload, options) {
        try {
            const token = jsonwebtoken_1.sign(payload, config_1.JWT_SECRET);
            return token;
        }
        catch (error) {
            throw new routing_controllers_1.BadRequestError(error.message);
        }
    }
    static verifyToken(token) {
        try {
            const isVerified = jsonwebtoken_1.verify(token, config_1.JWT_SECRET);
            return isVerified;
        }
        catch (error) {
            throw new routing_controllers_1.UnauthorizedError(error.message);
        }
    }
    static async encryptPassword(data) {
        try {
            const ciphertext = crypto_ts_1.AES.encrypt(data, config_1.AES_SECRET).toString();
            return ciphertext;
        }
        catch (error) {
            throw new routing_controllers_1.BadRequestError(error.message);
        }
    }
    static async compareDecryptedKey(password, hash) {
        try {
            const bytes = crypto_ts_1.AES.decrypt(hash, config_1.AES_SECRET);
            if (bytes.toString(crypto_ts_1.enc.Utf8) === password) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (error) {
            throw new routing_controllers_1.BadRequestError(error.message);
        }
    }
}
exports.Utils = Utils;
//# sourceMappingURL=utils.js.map