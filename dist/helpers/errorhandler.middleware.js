"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomErrorHandler = void 0;
const routing_controllers_1 = require("routing-controllers");
const class_validator_1 = require("class-validator");
const logger_1 = require("../logger");
const moment = require("moment");
let CustomErrorHandler = class CustomErrorHandler {
    error(error, request, response, next) {
        const statusCode = !!error.httpCode ? error.httpCode : response.statusCode;
        if (!!error.errors && class_validator_1.isArray(error.errors)) {
            const validationErrors = error.errors.reduce((acc, currentElement) => {
                if (currentElement.children.length > 0 && class_validator_1.isArray(currentElement.children)) {
                    acc.push(...processChildErrors(currentElement.children, currentElement.property));
                }
                else {
                    acc.push({
                        parent_property: null,
                        property: currentElement.property,
                        constraints: Object.values(currentElement.constraints).join(" & ")
                    });
                }
                return acc;
            }, []);
            const errorObj = {
                statusCode,
                date: moment().format('YYYY-MM-DD hh:mm'),
                message: validationErrors,
                request_url: request.url,
                request_data: request.method === "GET" ? request.query || request.params : request.body,
                trace: error.stack
            };
            logger_1.logger.error(errorObj);
            return response.status(statusCode).json(validationErrors);
        }
        const errorObj2 = {
            statusCode,
            date: moment().format('YYYY-MM-DD hh:mm'),
            message: error.message,
            request_url: request.url,
            request_data: request.method === "GET" ? request.query || request.params : request.body,
            trace: error.stack
        };
        logger_1.logger.error(errorObj2);
        const Error = {
            status: false,
            message: error.message,
        };
        response.status(statusCode).json(Error);
    }
};
CustomErrorHandler = __decorate([
    routing_controllers_1.Middleware({ type: "after" })
], CustomErrorHandler);
exports.CustomErrorHandler = CustomErrorHandler;
const processChildErrors = (errArr, parent) => {
    const errorMessageArr = errArr.reduce((acc, current) => {
        if (current.children.length > 0) {
            acc.push(...processChildErrors(current.children, parent));
        }
        else {
            acc.push({
                parent_property: parent,
                property: current.property,
                message: Object.values(current.constraints).join(" & ")
            });
        }
        return acc;
    }, []);
    return errorMessageArr;
};
//# sourceMappingURL=errorhandler.middleware.js.map