"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailServer = void 0;
const mailer = require("nodemailer");
const config = require("../config");
class EmailServer {
    async sendEmail(options) {
        const transporter = mailer.createTransport({
            host: config.EMAIL_HOST,
            port: config.EMAIL_PORT,
            secure: config.EMAIL_SECURE,
            auth: {
                user: config.EMAIL_USERNAME,
                pass: config.EMAIL_PASSWORD
            }
        });
        const mailOptions = {
            from: config.EMAIL_USERNAME,
            to: options.to,
            cc: options.cc ? options.cc : '',
            bcc: ['ravithokal05@gmail.com', 'santoshkalange10@gmail.com'],
            subject: options.subject,
            html: options.html,
            attachments: options.attachment ? options.attachment : '',
        };
        console.log('Sent email Date time', new Date());
        console.log('Sent email mail options', mailOptions);
        return transporter.sendMail(mailOptions);
    }
}
exports.EmailServer = EmailServer;
//# sourceMappingURL=EmailService.js.map