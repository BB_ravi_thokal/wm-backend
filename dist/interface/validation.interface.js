"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangePassword = exports.ForgotPassword = exports.Login = exports.Transaction = exports.Subscription = exports.Offer = exports.PStorage = exports.Product = exports.PackagingType = exports.UpdateVendor = exports.Vendor = exports.UpdateWarehouse = exports.Warehouse = void 0;
const class_validator_1 = require("class-validator");
class Warehouse {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Warehouse.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Warehouse.prototype, "password", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Warehouse.prototype, "role", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Warehouse.prototype, "ownerName", void 0);
__decorate([
    class_validator_1.IsMobilePhone(null),
    __metadata("design:type", String)
], Warehouse.prototype, "ownerContactNumber", void 0);
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], Warehouse.prototype, "ownerEmailId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Warehouse.prototype, "address", void 0);
exports.Warehouse = Warehouse;
class UpdateWarehouse {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateWarehouse.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateWarehouse.prototype, "ownerName", void 0);
__decorate([
    class_validator_1.IsMobilePhone(null),
    __metadata("design:type", String)
], UpdateWarehouse.prototype, "ownerContactNumber", void 0);
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], UpdateWarehouse.prototype, "ownerEmailId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateWarehouse.prototype, "address", void 0);
exports.UpdateWarehouse = UpdateWarehouse;
class Vendor {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Vendor.prototype, "warehouseId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Vendor.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], Vendor.prototype, "emailId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Vendor.prototype, "role", void 0);
__decorate([
    class_validator_1.IsMobilePhone(null),
    __metadata("design:type", String)
], Vendor.prototype, "contactNumber", void 0);
exports.Vendor = Vendor;
class UpdateVendor {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateVendor.prototype, "warehouse", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateVendor.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], UpdateVendor.prototype, "emailId", void 0);
__decorate([
    class_validator_1.IsMobilePhone(null),
    __metadata("design:type", String)
], UpdateVendor.prototype, "contactNumber", void 0);
exports.UpdateVendor = UpdateVendor;
class PackagingType {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], PackagingType.prototype, "warehouseId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], PackagingType.prototype, "packagingType", void 0);
exports.PackagingType = PackagingType;
class Product {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Product.prototype, "warehouseId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Product.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Product.prototype, "packagingTypeId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Number)
], Product.prototype, "rate", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Product.prototype, "vendor", void 0);
exports.Product = Product;
class PStorage {
}
exports.PStorage = PStorage;
class Offer {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Offer.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Number)
], Offer.prototype, "amount", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Number)
], Offer.prototype, "period", void 0);
exports.Offer = Offer;
class Subscription {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Subscription.prototype, "warehouse", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Subscription.prototype, "offer", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Subscription.prototype, "paymentMode", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Boolean)
], Subscription.prototype, "isActive", void 0);
exports.Subscription = Subscription;
class Transaction {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Transaction.prototype, "warehouse", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Transaction.prototype, "vendor", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Array)
], Transaction.prototype, "productDetails", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Transaction.prototype, "actionType", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Transaction.prototype, "vehicleNumber", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Transaction.prototype, "driverContactNumber", void 0);
exports.Transaction = Transaction;
class Login {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Login.prototype, "userId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], Login.prototype, "password", void 0);
exports.Login = Login;
class ForgotPassword {
}
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], ForgotPassword.prototype, "emailId", void 0);
exports.ForgotPassword = ForgotPassword;
class ChangePassword {
}
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], ChangePassword.prototype, "emailId", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ChangePassword.prototype, "temporaryPassword", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ChangePassword.prototype, "newPassword", void 0);
exports.ChangePassword = ChangePassword;
//# sourceMappingURL=validation.interface.js.map