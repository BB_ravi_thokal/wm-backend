"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const routing_controllers_1 = require("routing-controllers");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const response_interceptor_1 = require("./helpers/response.interceptor");
const utils_1 = require("./helpers/utils");
const config_1 = require("./config");
const typedi_1 = require("typedi");
routing_controllers_1.useContainer(typedi_1.Container);
try {
    mongoose.connect(config_1.MONGO_ATLAS_URL, {
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    });
    console.log('database connection success', config_1.MONGO_ATLAS_URL);
}
catch (err) {
    console.log('database connection error', err);
}
const expressApp = routing_controllers_1.createExpressServer({
    controllers: [__dirname + "/controllers/**/*.{ts,js}"],
    cors: {
        origin: (origin, callback) => {
            !!origin ? callback(null, origin) : callback(null, "*");
        },
        methods: ["GET", "POST", "OPTIONS", "PUT", "PATCH", "DELETE"],
        allowedHeaders: [
            "authorization",
            "Authorization",
            "X-Requested-With",
            "content-type",
        ],
        credentials: true,
        optionsSuccessStatus: 200,
    },
    interceptors: [response_interceptor_1.ResponseInterceptor],
    authorizationChecker: async (action, roles) => {
        const token = action.request.headers.authorization;
        const verifyToken = utils_1.Utils.verifyToken(token);
        if (verifyToken) {
            console.log('token verified success');
            return true;
        }
        else {
            return false;
        }
    },
    currentUserChecker: async (action) => {
        const token = action.request.headers.authorization;
        const user = utils_1.Utils.verifyToken(token);
        return user;
    },
});
expressApp.use(bodyParser.urlencoded({ limit: "100mb", extended: false }));
exports.default = expressApp;
//# sourceMappingURL=app.js.map