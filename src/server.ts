import expressApp from "./app";
import { logger } from "./logger";
import { JobService } from './services/cron-job.service';
// const {
// 	SERVER: { PORT },
// }: any = get("APP");

// tslint:disable-next-line:no-empty
expressApp.listen(process.env.PORT || 4000, () => {
    // tslint:disable-next-line:no-console
    console.log('Server started on port 4000 .....');
    const jobService = new JobService();
    // jobService.GetDataFromMonthBill();
	jobService.dailyRunner();
	jobService.monthlyBillGenerator();

});
