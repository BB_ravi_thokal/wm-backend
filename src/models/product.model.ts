import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const productSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    name: { type: String, require: true },
    packagingType: { type: Schema.Types.ObjectId, require: true, ref: 'PackagingType' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    storage: { type: Schema.Types.ObjectId, ref: 'ProductStorage' },
    isDeleted: { type: Boolean,  default: false },
    rate: { type: Number, require: true },
}, { timestamps: true });

export default mongoose.model('Product', productSchema, 'Product');