import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const storageSchema = new Schema({
    _id: Schema.Types.ObjectId,
    quantity: { type: Number, required: true }
}, { timestamps: true });

export default mongoose.model('ProductStorage', storageSchema, 'ProductStorage');