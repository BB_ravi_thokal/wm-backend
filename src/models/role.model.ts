import * as mongoose from 'mongoose'

const Schema = mongoose.Schema;

const roleSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    role: String,
    isDeleted: { type: Boolean,  default: false }
}, { timestamps: true });

export default mongoose.model('Role', roleSchema, 'Role');