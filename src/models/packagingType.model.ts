import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const packagingTypeSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    packagingType: { type: String, require: true },
    isDeleted: { type: Boolean,  default: false }
}, { timestamps: true });

export default mongoose.model('PackagingType', packagingTypeSchema, 'PackagingType');