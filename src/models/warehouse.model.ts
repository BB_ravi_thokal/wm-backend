import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const warehouseSchema = new Schema({
    _id: Schema.Types.ObjectId,
    auth: { type: Schema.Types.ObjectId, ref: 'Auth' },
    name: { type: String,  required: true },
    ownerName: { type: String,  required: true },
    ownerContactNumber: { type: String,  required: true },
    ownerEmailId: { type: String,  required: true },
    address: { type: String,  required: true },
    isActive: { type: Boolean,  default: false },
    subscription: [{subscription: { type: Schema.Types.ObjectId, ref: 'Subscription'} }],
    subscriptionEndDate: { type: Date },
    isDeleted: { type: Boolean,  default: false }
}, { timestamps: true });

export default mongoose.model('Warehouse', warehouseSchema, 'Warehouse');