import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const productVendorMapping = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    product: { type: Schema.Types.ObjectId, ref: 'Product' },
    rate: { type: Number, require: true },
    packagingType: { type: String, require: true },
    storage: { type: Schema.Types.ObjectId, ref: 'ProductStorage' },
    isDeleted: { type: Boolean,  default: false }
}, { timestamps: true });

export default mongoose.model('ProductVendorMapping', productVendorMapping, 'ProductVendorMapping');