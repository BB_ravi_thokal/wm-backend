import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const faultyProductSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    productDetails: { type: Object, required: true },
}, { timestamps: true });

export default mongoose.model('FaultyProduct', faultyProductSchema, 'FaultyProduct');