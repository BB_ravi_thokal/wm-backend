import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const transactionSchema = new Schema({
    _id: Schema.Types.ObjectId,
    warehouse: { type: Schema.Types.ObjectId, ref: 'Warehouse' },
    vendor: { type: Schema.Types.ObjectId, ref: 'Vendor' },
    productDetails: { type: Array, required: true },
    DONumber: String,
    actionType: { type: String, required: true },
    vehicleNumber: { type: String, required: true },
    driverContactNumber: { type: String, required: true },
    messageBy: { type: String },
    isFaulty: { type: Boolean },
    faultyProduct: { type: Schema.Types.ObjectId, ref: 'FaultyProduct' },
    isDeleted: { type: Boolean,  default: false },
    exportedFrom: { type: Array, required: true },
    uniqueBillId: { type: Number }
}, { timestamps: true });

export default mongoose.model('Transaction', transactionSchema, 'Transaction');