import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const saveMonthlyBillSchema = new Schema({
    _id: Schema.Types.ObjectId,
    data: { type: Array }
}, { timestamps: true });

export default mongoose.model('SaveMonthBill', saveMonthlyBillSchema, 'SaveMonthBill');
