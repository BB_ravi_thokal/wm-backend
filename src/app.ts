import "reflect-metadata"; // this shim is required
import { createExpressServer, useContainer, Action } from "routing-controllers";
import * as mongoose from 'mongoose';
import * as bodyParser from "body-parser";
import { ResponseInterceptor } from './helpers/response.interceptor';
import { CustomErrorHandler } from "./middlewares/errorhandler.middleware";
import { Utils } from './helpers/utils';
import { MONGO_ATLAS_URL } from './config';
import { Container } from 'typedi';

useContainer(Container);

try {
	mongoose.connect(
		MONGO_ATLAS_URL,
		{
			useNewUrlParser: true,
			useFindAndModify: false,
			useUnifiedTopology: true
		}
	);
	// tslint:disable-next-line:no-console
	console.log('database connection success', MONGO_ATLAS_URL);
} catch(err) {
	// tslint:disable-next-line:no-console
	console.log('database connection error', err);
}




const expressApp = createExpressServer({
	controllers: [__dirname + "/controllers/**/*.{ts,js}"],
	cors: {
		origin: (origin: any, callback: any) => {
			!!origin ? callback(null, origin) : callback(null, "*");
		},
		methods: ["GET", "POST", "OPTIONS", "PUT", "PATCH", "DELETE"],
		allowedHeaders: [
			"authorization",
			"Authorization",
			"X-Requested-With",
			"content-type",
		],
		credentials: true,
		optionsSuccessStatus: 200,
	},
	// middlewares: [CustomErrorHandler],
	interceptors: [ResponseInterceptor],
	authorizationChecker: async (action: Action, roles: string[]) => {
		const token = action.request.headers.authorization;
		const verifyToken = Utils.verifyToken(token);
		if (verifyToken) {
			// tslint:disable-next-line:no-console
			console.log('token verified success');
			return true;
		} else {
			return false;
		}
    },
	// middlewares: [CustomErrorHandler],
	// interceptors: [ResponseInterceptor],
	// defaultErrorHandler: false,
	currentUserChecker: async (action: Action) => {
		const token = action.request.headers.authorization;
		const user: any = Utils.verifyToken(token);
		// request["user"] = user;
		return user;
	},
});

expressApp.use(bodyParser.urlencoded({ limit: "100mb", extended: false }));


export default expressApp;
