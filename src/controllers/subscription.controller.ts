import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { Subscription } from '../interface/validation.interface';
import { SubscriptionService } from '../services/subscription.service';
import { WarehouseService } from '../services/warehouse.service';
import { OfferService } from '../services/offer.service';

@JsonController('/subscription')
export class SubscriptionController {
    constructor(
        private subscriptionService: SubscriptionService,
        private warehouseService: WarehouseService,
        private offerService: OfferService,
    ) {}

    @Post('/create')
    async createSubscription(@Body() req: Subscription) {
        const getWarehouseDetails = await this.warehouseService.getWarehouse(req.warehouse);
        const offer = await this.offerService.getOffer(req.offer);
        let lastActiveDate;
        let currentSubscriptionStartDate;
        let currentSubscriptionEndDate;
        if (getWarehouseDetails.length > 0) {
            if (getWarehouseDetails[0].isActive) {
                currentSubscriptionStartDate = new Date(getWarehouseDetails[0].subscriptionEndDate);
                const tempEndDate = new Date(getWarehouseDetails[0].subscriptionEndDate)
                currentSubscriptionEndDate = new Date(tempEndDate.setDate(tempEndDate.getDate() + offer[0].period));
                const tempLastActiveDate = new Date(getWarehouseDetails[0].subscriptionEndDate);
                lastActiveDate = new Date(tempLastActiveDate.setDate(tempLastActiveDate.getDate() + offer[0].period));
            } else {
                currentSubscriptionStartDate = new Date();
                currentSubscriptionEndDate = new Date(new Date().setDate(new Date().getDate() + parseInt(offer[0].period, 10)));
                lastActiveDate = new Date(new Date().setDate(new Date().getDate() + parseInt(offer[0].period, 10)));
            }
            const subscriptionObj = {
                warehouse: req.warehouse,
                offer: offer[0]._id,
                paymentMode: req.paymentMode,
                isActive: true,
                startDate: currentSubscriptionStartDate,
                endDate: currentSubscriptionEndDate
            }
            const subscription = await this.subscriptionService.createSubscription(subscriptionObj);
            const temp = getWarehouseDetails[0].subscription;
            temp.push({ subscription: subscription._id });
            // tslint:disable-next-line:no-console
            console.log('temp', temp);
            if (subscription) {
                const updateObject = {
                    _id: getWarehouseDetails[0]._id,
                    isActive: true,
                    subscription: temp,
                    subscriptionEndDate: lastActiveDate
                }
                const updateWarehouse = await this.warehouseService.updateWarehouse(updateObject);
                if (updateWarehouse) {
                    return {
                        message: 'Subscription created Successfully',
                        data: JSON.parse(JSON.stringify(subscription))
                    }
                } else {
                    throw new InternalServerError('Not able to create the subscription for you. Please try again later');
                }
            } else {
                throw new InternalServerError('Not able to create the subscription for you. Please try again later');
            }
        } else {
            throw new NotFoundError('Warehouse details not found');
        }
    }

    @Get('/')
    async getSubscription(@QueryParam('subscriptionId') id: string, @QueryParam('warehouseId') wId: string, @QueryParam('status') sts: boolean, ) {
        const getSubscription = await this.subscriptionService.getSubscription(id, wId, sts);
        if (getSubscription) {
            if (getSubscription.length > 0) {
                return {
                    message: 'Subscription fetch successfully',
                    data: JSON.parse(JSON.stringify(getSubscription))
                }
            } else {
                throw new NotFoundError('Subscription not found');
            }
        }
    }

}