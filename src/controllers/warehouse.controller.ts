import { BadRequestError, Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { WarehouseService } from '../services/warehouse.service';
import { Utils } from '../helpers/utils';
import { SharedService } from '../services/shared.service';
import { AuthService } from '../services/auth.service';
import { RoleService } from '../services/role.service';
import { SubscriptionService } from '../services/subscription.service';
import { Warehouse, UpdateWarehouse } from '../interface/validation.interface';

@JsonController('/warehouse')
export class WarehouseController {
    constructor(
        private warehouseService: WarehouseService,
        private sharedService: SharedService,
        private authService: AuthService,
        private roleService: RoleService,
        private subscriptionService: SubscriptionService
    ) { }

    @Post('/create')
    async createWarehouse(@Body() req: Warehouse) {
        const isUserExist = await this.sharedService.checkUserExist(req.ownerEmailId);
        if (isUserExist) {
            throw new UnauthorizedError('Email Id already exist');
        } else {
            const roleId = await this.roleService.getRoleIdByRole((req.role).toUpperCase());
            if (roleId) {
                const encryptPassword = await Utils.encryptPassword(req.password);
                const saveAuth = await this.authService.createAuth(req.ownerEmailId, roleId, encryptPassword);
                if (saveAuth) {
                    const createWarehouse = await this.warehouseService.createWarehouse(req, saveAuth._id);
                    if (createWarehouse) {
                        return JSON.parse(JSON.stringify({
                            message: 'Warehouse created successfully',
                            data: createWarehouse
                        }));
                    }
                } else {
                    throw new InternalServerError('Not able to create warehouse due to server error');
                }
            } else {
                throw new InternalServerError('Not able to create warehouse due to server error');
            }
        }
    }

    @Get('/')
    @Authorized()
    async getWareHouse(@QueryParam('warehouseId')id: string) {
        const warehouse = await this.warehouseService.getWarehouse(id);
        if (warehouse) {
            if (warehouse) {
                return {
                    message: 'Warehouse fetch successfully',
                    data: JSON.parse(JSON.stringify(warehouse))
                }
            } else {
                throw new NotFoundError('Warehouse not found');
            }
        }
    }

    @Get('/check-active')
    @Authorized()
    async checkActiveWarehouse(@QueryParam('warehouseId')id: string) {
        const checkStatus = await this.subscriptionService.getSubscription(undefined, id, undefined);
        if (checkStatus.length > 0) {
            return {
                status: 'active',
            }
        } else {
            return {
                status: 'expired'
            }
        }
    }

    @Put('/update')
    @Authorized()
    async updateWarehouse(@Body() req: UpdateWarehouse) {
        const vendor = await this.warehouseService.updateWarehouse(req);
        if (vendor) {
            return {
                message: 'Vendor updated Successfully',
                data: JSON.parse(JSON.stringify(vendor))
            }
        } else {
            throw new NotFoundError('Vendor not found');
        }
    }

    @Put('/delete')
    @Authorized()
    async deleteWarehouse(@Body() req: UpdateWarehouse) {
        const role = await this.roleService.getRoleIdByRole(('WAREHOUSE').toUpperCase());
        const authData = {
            _id: req.auth,
            roleId: role,
            isDeleted: true
        }
        const deleteAuth = await this.authService.deleteAuth(authData);
        if (deleteAuth) {
            const vendor = await this.warehouseService.deleteWarehouse(req);
            if (vendor) {
                return {
                    message: 'Vendor deleted Successfully',
                    data: JSON.parse(JSON.stringify(vendor))
                }
            } else {
                throw new NotFoundError('vendor not found');
            }
        } else {
            throw new InternalServerError('Not able to remove Vendor now please try later');
        }
    }

}