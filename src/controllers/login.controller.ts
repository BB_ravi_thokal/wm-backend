import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put  } from 'routing-controllers';
import { Login, ForgotPassword, ChangePassword } from '../interface/validation.interface';
import { AuthService } from '../services/auth.service';
import { WarehouseService } from '../services/warehouse.service';
import { VendorService } from '../services/vendor.service';
import Vendor from '../models/vendor.model';
import Auth from '../models/auth.model';
import { Types } from "mongoose";
const { ObjectId } = Types;

@JsonController('/login')
export class LoginController {
    constructor(
        private authService: AuthService,
        private warehouseService: WarehouseService,
        private vendorService: VendorService
    ) {}

    @Post('/')
    async login(@Body() req: Login) {
        const data = await this.authService.login(req);
        if (data) {
            let temp;
            if (data.role === 'WAREHOUSE') {
                temp = await this.warehouseService.getWarehouse(undefined, data.authId);
            } else {
                temp = await await Vendor.find({auth: ObjectId(data.authId)}).populate('warehouse');
            }
            return {
                token: data.token,
                warehouseData: temp[0]
            };
        } else {
            throw new UnauthorizedError('Failed to login. Please confirm user id and password');
        }
    }

    @Post('/forgot-password')
    async forgotPassword(@Body() req: ForgotPassword) {
        const createTempPass = this.authService.createTemporaryPassword(req);
        if (createTempPass) {
            return 'Temporary password sent on your email'
        } else {
            throw new NotFoundError('Entered email is not registered with us');
        }
    }

    @Post('/change-password')
    async changePassword(@Body() req: ChangePassword) {
        const createTempPass = this.authService.changePassword(req);
        if (createTempPass) {
            return 'Temporary password sent on your email'
        } else {
            throw new NotFoundError('Entered email is not registered with us');
        }
    }

}