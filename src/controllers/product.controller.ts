import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { Product } from '../interface/validation.interface';
import { ProductService } from '../services/product.service';
import { TransactionService } from '../services/transaction.service';

@JsonController('/product')
@Authorized()
export class ProductController {
    constructor(
        private productService: ProductService,
        private transactionService: TransactionService
    ) { }

    @Post('/create')
    async createProduct(@Body() req: Product) {
        const isProductExist = await this.productService.checkProductExist(req);
        if (!isProductExist) {
            const createStorage = await this.productService.createStorage(0);
            if (createStorage) {
                const product = await this.productService.createProduct(req, createStorage._id);
                // tslint:disable-next-line:no-console
                console.log('product --->', product);
                if (product) {
                    // this.productService.createProductVendorMapping(req, product._id, createStorage._id);
                    this.transactionService.createMonthBill(req, product._id);
                    return product;
                } else {
                    throw new InternalServerError('Not able to save product try again later');
                }
            } else {
                throw new InternalServerError('Not able to save product try again later');
            }
        } else {
            throw new UnauthorizedError('Product already exist');
        }
    }

    @Get('/')
    async getProduct(@QueryParam('productId')pid: string, @QueryParam('warehouseId')wId: string, @QueryParam('packageTypeId')typeId: string,
         @QueryParam('vendorId')vId: string, @QueryParam('paginationObject')pagination: string) {
        const product = await this.productService.getProduct(pid, wId, typeId, vId, pagination);
        if (product) {
            if (product) {
                return product;
            } else {
                throw new NotFoundError('Product not found');
            }
        }
    }

    @Put('/update')
    async updateProduct(@Body() req: Product) {
        const product = await this.productService.updateProduct(req);
        if (product) {
            return {
                message: 'Product updated Successfully',
                data: JSON.parse(JSON.stringify(product))
            }
        } else {
            throw new NotFoundError('Product not found');
        }
    }

    @Put('/delete')
    async deleteProduct(@Body() req: any) {
        const product = await this.productService.deleteProduct(req);
        if (product) {
            return {
                message: 'Product deleted Successfully',
                data: JSON.parse(JSON.stringify(product))
            }
        } else {
            throw new NotFoundError('Product not found');
        }
    }

}