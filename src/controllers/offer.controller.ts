import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { Offer } from '../interface/validation.interface';
import { OfferService } from '../services/offer.service';

@JsonController('/offer')
@Authorized()
export class OfferController {
    constructor(
        private offerService: OfferService
    ) {}

    @Post('/create')
    async createOffer(@Body() req: Offer) {
        const isOfferExist = await this.offerService.checkOfferExist(req.name);
        if (isOfferExist) {
            throw new UnauthorizedError('Offer name already exist');
        } else {
            const offer = await this.offerService.createOffer(req);
            if (offer) {
                return {
                    message: 'Offer created Successfully',
                    data: JSON.parse(JSON.stringify(offer))
                }
            } else {
                throw new NotFoundError('offer not found');
            }
        }
    }

    @Get('/')
    async getOffer(@QueryParam('offerId') id: string) {
        const getOffer = await this.offerService.getOffer(id);
        if (getOffer) {
            if (getOffer.length > 0) {
                return {
                    message: 'Offer fetch successfully',
                    data: JSON.parse(JSON.stringify(getOffer))
                }
            } else {
                throw new NotFoundError('Offer not found');
            }
        }
    }

    @Put('/update')
    async updateOffer(@Body() req: Offer) {
        // tslint:disable-next-line:no-console
        console.log('in update controller');
        const offer = await this.offerService.updateOffer(req);
        if (offer) {
            return {
                message: 'Offer updated Successfully',
                data: JSON.parse(JSON.stringify(offer))
            }
        } else {
            throw new NotFoundError('Offer not found');
        }
    }

    @Put('/delete')
    async deleteOffer(@Body() req: any) {
        const offer = await this.offerService.deleteOffer(req);
        if (offer) {
            return {
                message: 'Offer deleted Successfully',
                data: JSON.parse(JSON.stringify(offer))
            }
        } else {
            throw new NotFoundError('offer not found');
        }
    }

}