import { BadRequestError, Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Delete, Authorized  } from 'routing-controllers';
import { RoleService } from '../services/role.service';

@JsonController('/role')
@Authorized()
export class RoleController {
    constructor(
        private roleService: RoleService
    ) { }

    @Post('/create')
    async createRole(@Body()req: any) {
        const isRoleExist =  await this.roleService.checkRolExist(req.role);
        if (isRoleExist) {
            throw new UnauthorizedError('Role already exists');
        } else {
            const create = await this.roleService.createRole(req);
            if (create) {
                return 'Role created successfully';
            } else {
                throw new InternalServerError('Not able to create role due to server error');
            }
        }
    }

    @Get('/')
    async getAllRole(@QueryParam('roleId')roleId: string)  {
        const role = await this.roleService.getRole(roleId);
        if (role) {
            return {
                message: 'Role fetched successfully',
                data: JSON.parse(JSON.stringify(role))
            }
        } else {
            throw new NotFoundError('Role not found');
        }
    }

    @Post('/delete')
    async deleteRole(@Body()req: any)  {
        const role = await this.roleService.deleteRole(req.roleId);
        if (role) {
            return "Role deleted successfully";
        } else {
            throw new NotFoundError('Role not found');
        }
    }
}