import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized, UploadedFile  } from 'routing-controllers';
import { Transaction } from '../interface/validation.interface';
import { TransactionService } from '../services/transaction.service';
import { ProductService } from '../services/product.service';

@JsonController('/transaction')
// @Authorized()
export class TransactionController {
    constructor(
        private transactionService: TransactionService,
        private productService: ProductService
    ) {}

    @Post('/create')
    async createTransaction(@Body() req: Transaction) {
        if (req.actionType.toLowerCase() === 'export') {
            let count = 0;
            let qty = 0
            let productName = '';
            for (const element of req.productDetails) {
                const storage = await this.productService.checkStorageAvailability(element.storageId);
                qty = storage.quantity;
                if (!(storage.quantity >= element.quantity)) {
                    count++;
                    productName = element.name;
                }
            }
            if (count > 0) {
                throw new UnauthorizedError('We have only ' + qty + ' storage package for ' + productName);
            } else {
                const transaction = await this.transactionService.createTransaction(req);
                if (transaction) {
                    this.transactionService.managePostTransaction(transaction._id, req);
                    this.productService.manageStorageAfterImpExp(req.productDetails, req.actionType);
                    return transaction;
                } else {
                    throw new InternalServerError('Not able to save the bill. Please try again later');
                }
            }
        } else {
            let addFaultyEntry: any;
                if (req.isFaulty) {
                    addFaultyEntry = await this.transactionService.createFaultyProduct(req);
                } else {
                    addFaultyEntry = {
                        _id: undefined
                    };
                }
            if (addFaultyEntry) {
                let transaction: any;
                if (addFaultyEntry._id) {
                    transaction = await this.transactionService.createTransaction(req, addFaultyEntry._id);
                } else {
                    transaction = await this.transactionService.createTransaction(req);
                }
                if (transaction) {
                    this.transactionService.managePostTransaction(transaction._id, req);
                    this.productService.manageStorageAfterImpExp(req.productDetails, req.actionType);
                    return transaction;
                } else {
                    throw new InternalServerError('Not able to save the bill. Please try again later');
                }
            } else {
                throw new InternalServerError('Not able to save the bill. Please try again later');
            }
        }
    }

    @Get('/')
    async getTransaction(@QueryParam('transactionId')tId: string, @QueryParam('warehouseId')wId: string,
         @QueryParam('vendorId')vId: string, @QueryParam('productId')pId: string, @QueryParam('paginationObject')pagination: any) {
        // console.log('pagination s and e date-->', pagination.startDate.toISOString, pagination.endDate.toISOString);
        const transaction = await this.transactionService.getTransaction(tId, wId, vId, pId, pagination, undefined);
        if (transaction) {
            if (transaction) {
                return transaction;
            } else {
                throw new NotFoundError('Transaction details not found');
            }
        }
    }

    @Get('/get-import')
    async getImport(@QueryParam('transactionId')tId: string, @QueryParam('warehouseId')wId: string, @QueryParam('vendorId')vId: string,
    @QueryParam('productId')pId: string, @QueryParam('paginationObject')pagination: any) {
        const transaction = await this.transactionService.getTransaction(tId, wId, vId, pId, pagination, 'import');
        if (transaction) {
            if (transaction) {
                return transaction;
            } else {
                throw new NotFoundError('Transaction details not found');
            }
        }
    }

    @Get('/get-export')
    async getExport(@QueryParam('transactionId')tId: string, @QueryParam('warehouseId')wId: string, @QueryParam('vendorId')vId: string,
    @QueryParam('productId')pId: string, @QueryParam('paginationObject')pagination: any) {
        const transaction = await this.transactionService.getTransaction(tId, wId, vId, pId, pagination, 'export');
        if (transaction) {
            if (transaction) {
                return transaction;
            } else {
                throw new NotFoundError('Transaction details not found');
            }
        }
    }

    @Put('/delete-transaction')
    async deleteTransaction(@Body() req: any) {
        if (req.actionType.toLowerCase() === 'import') {
            const checkImpSts = await this.transactionService.checkImportStatus(req._id);
            if (!checkImpSts) {
                const deleteImportTransaction = await this.transactionService.deleteImportTransaction(req);
                if (deleteImportTransaction) {
                    return 'Transaction deleted successfully';
                } else {
                    throw new UnauthorizedError('You cannot delete this entry as already some products are exported');
                }
            } else {
                throw new UnauthorizedError('You cannot delete this entry as already some products are exported');
            }
        } else {
            const deleteExport = await this.transactionService.deleteExportTransaction(req);
            if (deleteExport) {
                return 'Transaction deleted successfully';
            } else {
                throw new UnauthorizedError('Not able to delete this entry. Please try again later or contact admin');
            }
        }
    }

}