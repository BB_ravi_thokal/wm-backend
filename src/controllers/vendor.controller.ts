import { BadRequestError, Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { SharedService } from '../services/shared.service';
import { Utils } from '../helpers/utils';
import { AuthService } from '../services/auth.service';
import { RoleService } from '../services/role.service';
import { Vendor, UpdateVendor } from '../interface/validation.interface';
import * as generator from 'generate-password';
import { VendorService } from '../services/vendor.service';
import { WarehouseService } from '../services/warehouse.service';

@JsonController('/vendor')
@Authorized()
export class VendorController {
    constructor(
        private vendorService: VendorService,
        private warehouseService: WarehouseService,
        private sharedService: SharedService,
        private authService: AuthService,
        private roleService: RoleService
    ) { }

    @Post('/create')
    async createVendor(@Body() req: Vendor) {
        const isUserExist = await this.sharedService.checkUserExist(req.emailId);
        if (isUserExist) {
            throw new UnauthorizedError('Email Id already exist');
        } else {
            const roleId = await this.roleService.getRoleIdByRole((req.role).toUpperCase());
            if (roleId) {
                const password = generator.generate({
                    length: 10,
                    numbers: true
                });
                const encryptPassword = await Utils.encryptPassword(password);
                const saveAuth = await this.authService.createAuth(req.emailId, roleId, encryptPassword);
                if (saveAuth) {
                    const vendor = await this.vendorService.createVendor(req, saveAuth._id);
                    if (vendor) {
                        const warehouseDetails = await this.warehouseService.getWarehouse(vendor.warehouse);
                        this.vendorService.sendAccountCreationEmail(vendor, warehouseDetails[0]);
                        return vendor;
                    }
                } else {
                    throw new InternalServerError('Not able to create vendor due to server error');
                }
            } else {
                throw new InternalServerError('Not able to create vendor due to server error');
            }
        }
    }

    @Get('/')
    async getVendor(@QueryParam('warehouseId')wId: string, @QueryParam('vendorId')id: string,
        @QueryParam('paginationObject')pagination: any) {
        const vendor = await this.vendorService.getVendor(id, wId, undefined, pagination);
        if (vendor) {
            if (vendor) {
                return vendor;
            } else {
                throw new NotFoundError('Vendor not found');
            }
        }
    }

    @Put('/update')
    async updateVendor(@Body() req: UpdateVendor) {
        const vendor = await this.vendorService.updateVendor(req);
        if (vendor) {
            return vendor;
        } else {
            throw new NotFoundError('Vendor not found');
        }
    }

    @Put('/delete')
    async deleteVendor(@Body() req: UpdateVendor) {
        const role = await this.roleService.getRoleIdByRole(('VENDOR').toUpperCase());
        const getVendor = await this.vendorService.getVendor(req._id);
        if (getVendor) {
            const authData = {
                _id: getVendor[0].auth,
                roleId: role,
                isDeleted: true
            };
            const deleteAuth = await this.authService.deleteAuth(authData);
            if (deleteAuth) {
                const vendor = await this.vendorService.deleteVendor(req);
                if (vendor) {
                    return vendor;
                } else {
                    throw new NotFoundError('vendor not found');
                }
            } else {
                throw new InternalServerError('Not able to remove Vendor now please try later');
            }
        } else {
            throw new NotFoundError('vendor not found');
        }
    }

}