import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put  } from 'routing-controllers';
import { JobService } from '../services/cron-job.service';

@JsonController('/job')
export class LoginController {
    constructor(
        private jobService: JobService
    ) {}

    @Get('/generate-bill')
    async generateBill() {
        this.jobService.testFunction();
        return 'success';
    }

    @Get('/daily-runner')
    async dailyRunner() {
        return await this.jobService.testFunctionDaily();
    }
}