import { BadRequestError, Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { PackagingType } from '../interface/validation.interface';
import { PackagingTypeService } from '../services/packaging-type.service';

@JsonController('/packagingType')
@Authorized()
export class PackagingTypeController {
    constructor(
        private packagingTypeService: PackagingTypeService
    ) { }

    @Post('/create')
    async createPackagingType(@Body() req: PackagingType) {
        const isPackageExist = await this.packagingTypeService.checkPackagingTypeExist(req);
        if (isPackageExist) {
            throw new UnauthorizedError('Packaging type already exist');
        } else {
            const packagingType = await this.packagingTypeService.createPackagingType(req);
            if (packagingType) {
                return packagingType;
            } else {
                throw new InternalServerError('Not able to save packaging type try again later');
            }
        }
    }

    @Get('/')
    async getPackagingTYpe(@QueryParam('typeId')id: string, @QueryParam('warehouseId')wId: string) {
        const packagingType = await this.packagingTypeService.getPackagingType(id, wId);
        if (packagingType) {
            if (packagingType) {
                return packagingType;
            } else {
                throw new NotFoundError('packagingType not found');
            }
        }
    }

    @Put('/update')
    async updatePackagingType(@Body() req: PackagingType) {
        const packagingType = await this.packagingTypeService.updatePackagingType(req);
        if (packagingType) {
            return {
                message: 'PackagingType updated Successfully',
                data: JSON.parse(JSON.stringify(packagingType))
            }
        } else {
            throw new NotFoundError('PackagingType not found');
        }
    }

    @Put('/delete')
    async deletePackagingType(@Body() req: any) {
        const packagingType = await this.packagingTypeService.deletePackagingType(req);
        if (packagingType) {
            return {
                message: 'PackagingType deleted Successfully',
                data: JSON.parse(JSON.stringify(packagingType))
            }
        } else {
            throw new NotFoundError('PackagingType not found');
        }
    }

}