import { Body, Get, JsonController, Post, QueryParam, UploadedFiles } from "routing-controllers";
import { SendEmailService } from "../services/send-email.service";

@JsonController('/email')
export class SenEmailController {

    constructor(private sendEmailService: SendEmailService) { }

    @Post('/transaction')
    async sendImportExportEmail(@UploadedFiles("file") formData: any, @Body() body: any) {
        await this.sendEmailService.sendTransactionEmail(formData, JSON.parse(body.body));
        return 'success'
    }
}