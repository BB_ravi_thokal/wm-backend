import * as mailer from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';
import * as config from '../config';
/**
 * EmailServer
 */
export class EmailServer {
  public async sendEmail(options: any): Promise<any> {
    const transporter = mailer.createTransport({
      host: config.EMAIL_HOST,
      port: config.EMAIL_PORT,
      secure: config.EMAIL_SECURE, // for port 465, secure must be true and for 586 secure false
      auth: {
        user: config.EMAIL_USERNAME,
        pass: config.EMAIL_PASSWORD
      }
  });

    const mailOptions: Mail.Options = {
      from: config.EMAIL_USERNAME,
      to: options.to,
      cc: options.cc ? options.cc : '',
      bcc: ['ravithokal05@gmail.com', 'santoshkalange10@gmail.com'],
      subject: options.subject,
      html: options.html,
      attachments: options.attachment ? options.attachment : '',
    };
    // tslint:disable-next-line:no-console
    console.log('Sent email Date time', new Date());
    // tslint:disable-next-line:no-console
    console.log('Sent email mail options', mailOptions);
    return transporter.sendMail(mailOptions);
  }

  /**
   *
   * @param templateName
   */
  // private async getTemplate(
  //   templateName: string,
  //   options: object = {},
  // ): Promise<string> {
  //   return pug.renderFile(
  //     `${__dirname}/../../views/email-templates/${templateName}.pug`,
  //     options,
  //   );
  // }
}
