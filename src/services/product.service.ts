import Product from '../models/product.model';
import ProductStorage from '../models/storage.model';
import { PStorage } from '../interface/validation.interface';
import ProductVendorMapping from '../models/productVendorMapping.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
const { ObjectId } = Types;

export class ProductService {

    async createProduct(data: any, storageId: string): Promise<any> {
        const product = new Product({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouseId,
            name: data.name,
            packagingType: data.packagingTypeId,
            storage: storageId,
            rate: data.rate,
            vendor: data.vendor
        })
        return await product.save().then(res => {
            return res.populate('storage').populate('packagingType').populate('vendor');
        })
        .catch(err => {
            return false;
        });
    }

    async checkProductExist(productData: any): Promise<any> {
        const data = await Product.findOne({ name: {'$regex': productData.name,$options:'i'}, warehouse: ObjectId(productData.warehouseId), vendor: ObjectId(productData.vendor), isDeleted: false });
        if (data) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @description method to fetch packaging type based on the id
     * @param id product unique id
     * @param wId warehouse id
     * @param typeId packaging type Id
     */
    async getProduct(id: string, wId: string, typeId: string, vId: string, paginationObj: any): Promise<any> {
        const query: any = {
            isDeleted: false,
        };
        if (id) {
            query._id = id;
        } else if (typeId && wId && vId) {
            query.packagingType = typeId;
            query.warehouse = wId;
            query.vendor = vId;
        } else if (typeId && wId) {
            query.packagingType = typeId;
            query.warehouse = wId;
        } else if (wId && vId) {
            query.warehouse = wId;
            query.vendor = vId;
        } else if (wId) {
            query.warehouse = wId;
        } else if (typeId) {
            query.packagingType = typeId;
        }
        const allProduct = await Product.find(query)
            .countDocuments();

        let product;
        if (paginationObj !== '' && paginationObj !== undefined) {
            paginationObj = JSON.parse(paginationObj);
            product = await Product.find(query)
            .populate('storage').populate('packagingType').populate('vendor')
            .sort({createdAt: 'desc'})
            .skip(paginationObj.itemsPerPage * (paginationObj.currentPage - 1))
            .limit(paginationObj.itemsPerPage);

            return { result: product, count: allProduct };
        } else {
            product = await Product.find(query)
            .populate('storage').populate('packagingType').populate('vendor')
            .sort({createdAt: 'desc'});

            return { result: product, count: allProduct };
        }
    }

    async updateProduct(data: any): Promise<any> {
        if (data._id) {
            const product = await Product.updateOne({ _id: ObjectId(data._id) }, data);
            if (product.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async deleteProduct(data: any): Promise<any> {
        if (data._id) {
            // const mapping = await ProductVendorMapping.updateOne({ product: ObjectId(data._id) }, data);
            const product = await Product.updateOne({ _id: ObjectId(data._id) }, data);
            if (product.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async createStorage(qty: number): Promise<any> {
        const storage = new ProductStorage({
            _id: new mongoose.Types.ObjectId(),
            quantity: qty
        });
        // tslint:disable-next-line:no-console
        return await storage.save().then(res => {
            return res;
        })
        .catch(err => {
            return false;
        });
    }

    async updateStorage(data: PStorage): Promise<any> {
        if (data._id) {
            const storage = await ProductStorage.updateOne({ _id: ObjectId(data._id) }, data);
        }
    }

    async checkStorageAvailability(prodId: string) {
        const getStorage: any = await ProductStorage.findOne({ _id: ObjectId(prodId)});
        if (getStorage) {
            return getStorage;
        }
    }

    async manageStorageAfterImpExp(prodDetails: any, actionType: string) {
        for (const product of prodDetails) {
            const getStorage: any = await ProductStorage.findOne({ _id: ObjectId(product.storageId)});
            if (getStorage) {
                let updateObj;
                if (actionType.toLowerCase() === 'import') {
                    updateObj = {
                        _id: product.storageId,
                        quantity: ( parseFloat(getStorage.quantity) + parseFloat(product.quantity) )
                    }
                } else {
                    updateObj = {
                        _id: product.storageId,
                        quantity: (parseFloat(getStorage.quantity) - parseFloat(product.quantity) )
                    }
                }
                this.updateStorage(updateObj);
            }
        }
    }
 }