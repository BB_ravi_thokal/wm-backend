import Subscription from '../models/subscriptionDetails.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
const { ObjectId } = Types;

export class SubscriptionService {
    async createSubscription(data: any): Promise<any> {
        const subscription = new Subscription({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouse,
            offer: data.offer,
            paymentMode: data.paymentMode,
            isActive: data.isActive,
            startDate: data.startDate,
            endDate: data.endDate
        });
        const subRes = await subscription.save();
        if (subRes) {
            return subRes;
        } else {
            return false;
        }
    }

    /**
     * @description method to fetch packaging type based on the id
     * @param id packaging type unique id
     */
    async getSubscription(id: string, wId: string, sts: boolean): Promise<any> {
        if (id) {
            const subscription = await Subscription.find({_id: ObjectId(id)});
            return subscription;
        } else if (wId) {
            const subscription = await Subscription.find({ warehouse: ObjectId(wId), isActive: true });
            return subscription;
        } else if (sts) {
            const subscription = await Subscription.find({ isActive: sts });
            return subscription;
        } else if (!sts) {
            const subscription = await Subscription.find({ isActive: !sts });
            return subscription;
        } else {
            const subscription = await Subscription.find({ });
            return subscription;
        }
    }

    public static async deActivateSubscription(): Promise<any> {
        const subscription: any = await Subscription.find({ isActive: true });
        for (const element of subscription) {
            if (new Date(element.endDate) < new Date()) {
                await Subscription.updateOne({ _id: ObjectId(element._id) }, {isActive: false});
            }
        }
    }
 }