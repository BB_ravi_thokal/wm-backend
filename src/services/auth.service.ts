import Auth from '../models/auth.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
import { Utils } from '../helpers/utils';
import { UnauthorizedError, NotFoundError, InternalServerError } from 'routing-controllers';
import { EmailServer } from '../helpers/EmailService';
const { ObjectId } = Types;

export class AuthService {
    async createAuth(emailId: string, role: string, pass: string): Promise<any> {
        const auth = new Auth({
            _id: new mongoose.Types.ObjectId(),
            roleId: role,
            userId: emailId,
            password: pass
        })
        const authRes = await auth.save();
        if (authRes) {
            return authRes;
        } else {
            return false;
        }
    }

    async deleteAuth(data: any): Promise<any> {
        if (data._id) {
            const auth = await Auth.updateOne({ _id: ObjectId(data._id) }, data);
            if (auth.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async getAuthByUserId(id: string): Promise<any> {
        if (id) {
            const auth = await Auth.findOne({ userId: id, isDeleted: false });
            if (auth) {
                return auth;
            } else {
                return false;
            }
        }
    }

    async login(data: any): Promise<any> {
        const getAuth: any = await Auth.findOne({ userId: data.userId, isDeleted: false }).populate('roleId');
        if (getAuth) {
            const checkPassword = await Utils.compareDecryptedKey(data.password, getAuth.password);
            if (checkPassword) {
                const payload = {
                    role: getAuth.roleId.role,
                    userId: data.userId
                }
                const getToken = Utils.createJWTToken(payload);
                return {
                    authId: getAuth._id,
                    token: getToken,
                    role: getAuth.roleId.role
                };
            } else {
                throw new UnauthorizedError('Please enter correct password');
            }
        } else {
            throw new NotFoundError('The mentioned user id does not exist');
        }
    }

    async createTemporaryPassword(data: any) {
        const checkUser = await this.getAuthByUserId(data.emailId);
        if (checkUser) {
            const random = Math.floor(Math.random() * Math.floor(999999));
            const encryptPassword = await Utils.encryptPassword(random.toString());
            const auth = await Auth.updateOne({ _id: ObjectId(checkUser._id) }, { temporaryPassword: encryptPassword });
            if (auth.nModified > 0) {
                const mailBody: any = {
                    to: data.emailId,
                    subject: 'Transaction successful',
                    html: '<div><p>Greetings from Warehouse Management system.</p></br><p> Please use temporary password:'
                    + random +
                    ' for login in system.</p></br><p>Please change the password immediately for security reasons</p></div></br><a href="http://3.135.198.16/dw-home">Warehouse Management System Website</a>',
                };
                const mailStatus = await new EmailServer().sendEmail(mailBody);
                return true;
            } else {
                throw new InternalServerError('Not able to create temporary password for you. Please try again later');
            }
        } else {
            return false;
        }
    }

    async changePassword(data: any) {
        const checkUser = await this.getAuthByUserId(data.emailId);
        if (checkUser) {
            const checkPassword = await Utils.compareDecryptedKey(data.temporaryPassword, checkUser.temporaryPassword);
            if (checkPassword) {
                const encryptPassword = await Utils.encryptPassword(data.newPassword);
                const auth = await Auth.updateOne({ _id: ObjectId(checkUser._id) }, { temporaryPassword: '', password: encryptPassword });
                if (auth.nModified > 0) {
                    return true;
                } else {
                    throw new InternalServerError('Not able to change the password. Please try again later');
                }
            } else {
                throw new UnauthorizedError('Entered temporary password is wrong');
            }
        } else {
            return false;
        }
    }
 }