import Vendor from '../models/vendor.model';
import Auth from '../models/auth.model';
import { Types } from "mongoose";
const { ObjectId } = Types;
import * as mongoose from 'mongoose';
import { EmailServer } from '../helpers/EmailService';
import { Utils } from '../helpers/utils';

export class VendorService {
    /**
     * @description method to create new vendor under particular warehouse
     * @param data request object containing vendor data
     * @param authId reference of auth table
     */
    async createVendor(data: any, authId: string): Promise<any> {
        const vendor = new Vendor({
            _id: new mongoose.Types.ObjectId(),
            auth: authId,
            warehouse: data.warehouseId,
            name: data.name,
            emailId: data.emailId,
            contactNumber: data.contactNumber,
            TotalAmountPaid: 0,
            pendingDues: 0,
        });
        return await vendor.save().then(res => {
            if (res) {
                return res;
            } else {
                return false;
            }
        });
    }

    async sendAccountCreationEmail(vendorData: any, warehouseData: any) {
        const random = Math.floor(Math.random() * Math.floor(999999));
        const encryptPassword = await Utils.encryptPassword(random.toString());
        await Auth.updateOne({ _id: ObjectId(vendorData.auth) }, { temporaryPassword: encryptPassword });
        const mailBody: any = {
            to: vendorData.emailId,
            cc: warehouseData.ownerEmailId,
            subject: 'New account created',
            html: '<div><p>Welcome to Warehouse Management system.</p></br><p>' + warehouseData.name +' has added you as a vendor.</p></br><p> Please use temporary password:'
            + random +
            ' for login in system</p></div></br><a href="http://3.135.198.16/dw-home">Warehouse Management System Website</a>'
        };
        await new EmailServer().sendEmail(mailBody);
    }

    /**
     * @description method to fetch vendors based on the vendor Id or warehouseId and all
     * @param id vendor unique id
     * @param warehouseId warehouse id
     */
    async getVendor(id?: string, warehouseId?: string, authId?: string, paginationObj?: any): Promise<any> {
        const query: any = {
            isDeleted: false
        }
        if (id) {
            query._id = id;
        } else if (warehouseId) {
            query.warehouse = warehouseId;
        } else if (authId) {
            query.auth = authId;
        }
        const allVendors = await Vendor.find(query).countDocuments();
        let vendors;
        if (paginationObj !== '' && paginationObj !== undefined) {
            vendors = await Vendor.find(query).populate('warehouse')
                .sort({createdAt: 'desc'})
                .skip(paginationObj.itemsPerPage * (paginationObj.currentPage - 1))
                .limit(paginationObj.itemsPerPage);

            return { result: vendors, count: allVendors };
        } else {
            vendors = await Vendor.find(query).populate('warehouse')
                .sort({createdAt: 'desc'});

            return { result: vendors, count: allVendors };
        }
    }

    async updateVendor(data: any): Promise<any> {
        if (data._id) {
            const vendor = await Vendor.updateOne({ _id: ObjectId(data._id) }, data);
            if (vendor.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async deleteVendor(data: any): Promise<any> {
        if (data._id) {
            const vendor = await Vendor.updateOne({ _id: ObjectId(data._id) }, data);
            if (vendor.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}