import Auth from '../models/auth.model';

export class SharedService {
    async checkUserExist(email: string): Promise<any> {
        const data = await Auth.findOne({ userId: {'$regex': email,$options:'i'}, isDeleted: false });
        if (data) {
            return data;
        } else {
            return false;
        }
    }
 }