import Role from '../models/role.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
const { ObjectId } = Types;
import { UnauthorizedError, InternalServerError, NotFoundError } from 'routing-controllers';

export class RoleService {
    async getRoleIdByRole(roleName: string): Promise<any> {
        const role = await Role.findOne({ role: roleName });
        if (role) {
            return role._id;
        } else {
            return false;
        }
    }

    async createRole(data: any): Promise<any> {
        const role = new Role({
            _id: new mongoose.Types.ObjectId(),
            role: data.role
        })
        return await role.save();
    }

    async checkRolExist(roleName: string): Promise<any> {
        return await Role.findOne({ role: roleName });
    }

    async getRole(id: any): Promise<any> {
        if (id) {
            const role = await Role.find({_id: ObjectId(id)});
            return role;
        } else {
            const role = await Role.find({});
            return role;
        }
    }

    async deleteRole(id: any): Promise<any> {
        if (id) {
            const role = await Role.deleteOne({_id: ObjectId(id)});
            return role;
        }
    }
 }