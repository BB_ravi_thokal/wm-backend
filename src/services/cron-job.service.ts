import * as mongoose from 'mongoose';
import * as schedule from 'node-schedule';
import Transaction from '../models/transaction.model';
import Warehouse from '../models/warehouse.model';
import Vendor from '../models/vendor.model';
import Import from '../models/imports.model';
import MonthBill from '../models/monthBill.model';
import SaveMonthBill from '../models/saveMonthBill.model';
import ProductStorage from '../models/storage.model';
import ProductModel from '../models/product.model';
import { Types } from "mongoose";
// import { Product } from '../interface/validation.interface';
import { Render } from 'routing-controllers';
import * as handlebars from 'handlebars';
import * as fs from 'fs';
import { EmailServer } from '../helpers/EmailService';
import * as jsDom from 'jsdom';
import { SubscriptionService } from './subscription.service';
const { ObjectId } = Types;

const document = new jsDom.JSDOM('html').window.document;
export class JobService {
    public async dailyRunner() {
        const rule = new schedule.RecurrenceRule();
        rule.dayOfWeek = [0, new schedule.Range(0, 6)];
        rule.hour = 4;
        rule.minute = 1;
        // rule.minute = 4;
        // tslint:disable-next-line:only-arrow-functions
        const job = schedule.scheduleJob(rule, async function () {
            // tslint:disable-next-line:no-console
            console.log('daily runner called', new Date());
            const getImports: any = await Import.find({ status: { $ne: 'Completed' }, expiryDate: { $lt: new Date() }, isDeleted: false });
            // tslint:disable-next-line:no-console
            if (getImports.length > 0) {
                for (const element of getImports) {
                    const expDate = element.expiryDate;
                    const updateObjImport = {
                        _id: element._id,
                        expiryDate: new Date(expDate.setDate(new Date(expDate).getDate() + 30)),
                    }
                    await Import.updateOne({ _id: element._id }, updateObjImport);
                    const getMonthBill: any = await MonthBill.findOne({ product: ObjectId(element.product) });
                    const getRate: any = await ProductModel.findOne({ _id: ObjectId(element.product), vendor: ObjectId(element.vendor), warehouse: ObjectId(element.warehouse)});
                    const updateObj = {
                        _id: getMonthBill._id,
                        quantity: getMonthBill.quantity + element.pendingQty,
                        amount: getMonthBill.amount + (getRate.rate * element.pendingQty)
                    }
                    await MonthBill.updateOne({ _id: getMonthBill._id }, updateObj);
                }
            }
            SubscriptionService.deActivateSubscription();
            const mailBody: any = {
                to: 'warehousemanagesystem@gmail.com',
                // cc: element.ownerEmailId,
                subject: 'Daily runner called',
                // html: htmlToSend,
            };
            await new EmailServer().sendEmail(mailBody);
        });
    }

    public async monthlyBillGenerator() {
        const rule = new schedule.RecurrenceRule();
        // rule.dayOfWeek = [0, new schedule.Range(0, 6)];
        rule.date = 1;
        rule.hour = 2;
        rule.minute = 10;
        const billDetails = [];
        // tslint:disable-next-line:no-console
        // tslint:disable-next-line:only-arrow-functions
        const job = schedule.scheduleJob(rule, async function () {
            // tslint:disable-next-line:no-console
            console.log('Monthly runner called', new Date());
            try {
                const getWarehouse: any = await Warehouse.find({ isActive: true, isDeleted: false });
                const billData = [];
                // tslint:disable-next-line:no-console
                // console.log('getWarehouse', getWarehouse);
                for (const warehouseObj of getWarehouse) {
                    const getVendor: any = await Vendor.find({ warehouse: ObjectId(warehouseObj._id), isDeleted: false });
                    // tslint:disable-next-line:no-console
                    // console.log('warehouse ID and Vendor', warehouseObj._id, getVendor);
                    for (const vendorObj of getVendor) {
                        const getProduct: any = await ProductModel.find({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), isDeleted: false }).populate('packagingType');
                        let productDetails = [];
                        let totalAmount = 0.00;
                        let totalQty = 0;
                        // tslint:disable-next-line:no-console
                        // console.log('warehouse ID, vendorID and product', warehouseObj._id, vendorObj._id, getProduct);
                        for (const productObj of getProduct) {
                            const getMonthBill: any = await MonthBill.findOne({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), product: ObjectId(productObj._id) });
                            // console.log('getMonthBill', getMonthBill);
                            if (getMonthBill.amount > 0) {
                                const tempProd = {
                                    productName: productObj.name,
                                    packagingType: productObj.packagingType.packagingType,
                                    amount: getMonthBill.amount,
                                    quantity: getMonthBill.quantity
                                };
                                // console.log('totalAmount', totalAmount);
                                // console.log('getMonthBill.amount', tempProd);
                                totalAmount = totalAmount + tempProd.amount;
                                totalQty = totalQty + tempProd.quantity;
                                productDetails.push(tempProd);
                            }
                        }
                        if (productDetails.length > 0) {
                            const temp = {
                                warehouseId: warehouseObj._id,
                                warehouseName: warehouseObj.name,
                                warehouseOwnerName: warehouseObj.ownerName,
                                ownerContactNumber: warehouseObj.ownerContactNumber,
                                ownerEmailId: warehouseObj.ownerEmailId,
                                vendorName: vendorObj.name,
                                vendorEmail: vendorObj.emailId,
                                vendorContactNumber: vendorObj.contactNumber,
                                products: productDetails,
                                totalAmt: totalAmount,
                                totalQuantity: totalQty
                            };
                            // tslint:disable-next-line:no-console
                            // console.log('final in vendor', temp);
                            billData.push(temp);
                            productDetails = [];
                        }
                        // this.sendEmail();
                    }
                }
                // tslint:disable-next-line:no-console
                console.log('billData-------------------->', billData);
                if (billData.length > 0) {
                    await this.readHTMLFile(billData);
                    const details = new SaveMonthBill({
                        _id: new mongoose.Types.ObjectId(),
                        data: billData
                    });
                    await details.save();
                    await MonthBill.updateMany({}, { quantity: 0, amount: 0 });
                }
            } catch (err) {
                // tslint:disable-next-line:no-console
                console.log('error', err);
            }
        }.bind(this));
    }

    async GetDataFromMonthBill() {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        console.log('firstDay, lastDay --->', firstDay, lastDay);
        const getMonthBill: any = await SaveMonthBill.find({ createdAt: { $gt: new Date(firstDay), $lt: new Date(lastDay)}  });
        console.log('getMonthBill --->', getMonthBill[0].data);
        // await MonthBill.updateMany({}, { quantity: 0, amount: 0 });
        // await ProductStorage.updateMany({}, {quantity: 0});
        // await this.readHTMLFile(getMonthBill[0].data);
    }

    async testFunctionDaily() {
        const getImports: any = await Import.find({ status: { $ne: 'Completed' }, expiryDate: { $lt: new Date() }, isDeleted: false });
        // tslint:disable-next-line:no-console
        console.log('imports array details', getImports);
        if (getImports.length > 0) {
            // tslint:disable-next-line:no-console
            console.log('imports array', getImports);
            for (const element of getImports) {
                const expDate = element.expiryDate;
                const updateObjImport = {
                    _id: element._id,
                    expiryDate: new Date(expDate.setDate(new Date(expDate).getDate() + 30)),
                }
                await Import.updateOne({ _id: element._id }, updateObjImport);
                const getMonthBill: any = await MonthBill.findOne({ product: ObjectId(element.product) });
                const getRate: any = await ProductModel.findOne({ _id: ObjectId(element.product), vendor: ObjectId(element.vendor), warehouse: ObjectId(element.warehouse)});
                const updateObj = {
                    _id: getMonthBill._id,
                    quantity: getMonthBill.quantity + element.pendingQty,
                    amount: getMonthBill.amount + (getRate.rate * element.pendingQty)
                }
                await MonthBill.updateOne({ _id: getMonthBill._id }, updateObj);
            }
        }
        return getImports;
    }

    async testFunction() {
        try {
            const getWarehouse: any = await Warehouse.find({ isActive: true, isDeleted: false });
            const billData = [];
            for (const warehouseObj of getWarehouse) {
                const getVendor: any = await Vendor.find({ warehouse: ObjectId(warehouseObj._id), isDeleted: false });
                for (const vendorObj of getVendor) {
                    const getProduct: any = await ProductModel.find({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), isDeleted: false }).populate('packagingType');
                    let productDetails = [];
                    let totalAmount = 0.00;
                    let totalQty = 0;
                    for (const productObj of getProduct) {
                        const getMonthBill: any = await MonthBill.findOne({ warehouse: ObjectId(warehouseObj._id), vendor: ObjectId(vendorObj._id), product: ObjectId(productObj._id) });
                        if (getMonthBill.amount > 0) {
                            const tempProd = {
                                productName: productObj.name,
                                packagingType: productObj.packagingType.packagingType,
                                amount: getMonthBill.amount,
                                quantity: getMonthBill.quantity
                            };
                            totalAmount = totalAmount + tempProd.amount;
                            totalQty = totalQty + tempProd.quantity;
                            productDetails.push(tempProd);
                        }
                    }
                    if (productDetails.length > 0) {
                        const temp = {
                            warehouseName: warehouseObj.name,
                            warehouseOwnerName: warehouseObj.ownerName,
                            ownerContactNumber: warehouseObj.ownerContactNumber,
                            ownerEmailId: warehouseObj.ownerEmailId,
                            vendorName: vendorObj.name,
                            vendorEmail: vendorObj.emailId,
                            vendorContactNumber: vendorObj.contactNumber,
                            products: productDetails,
                            totalAmt: totalAmount,
                            totalQuantity: totalQty
                        };
                        billData.push(temp);
                        productDetails = [];
                    }
                }
            }
            // tslint:disable-next-line:no-console
            console.log('billData-------------------->', billData);
            if (billData.length > 0) {
                this.readHTMLFile(billData);
            }
        } catch (err) {
            // tslint:disable-next-line:no-console
            console.log('error', err);
        }
        await MonthBill.updateMany({}, { quantity: 0, amount: 0 });
    }

    public async readHTMLFile(data: any) {
        await fs.readFile('./src/html/monthly-bill-report.html', { encoding: 'utf-8' }, async (err, html) => {
            if (err) {
                return err;
            }
            else {
                let count = 0;
                for (const element of data) {
                    count++;
                    // tslint:disable-next-line:no-console
                    // console.log('in rea html', html);
                    // tslint:disable-next-line:no-console
                    console.log('email sent to', count);
                    const template = handlebars.compile(html);
                    const table = document.createElement('table');
                    const tr1 = document.createElement('tr');
                    const th1 = document.createElement('th');
                    const th2 = document.createElement('th');
                    const th3 = document.createElement('th');
                    const th4 = document.createElement('th');
                    const th5 = document.createElement('th');

                    const header1 = document.createTextNode('Product Name');
                    const header2 = document.createTextNode('Packaging Type');
                    const header3 = document.createTextNode('Quantity');
                    const header4 = document.createTextNode('Rate');
                    const header5 = document.createTextNode('Amount');

                    th1.appendChild(header1);
                    th2.appendChild(header2);
                    th3.appendChild(header3);
                    th4.appendChild(header4);
                    th5.appendChild(header5);

                    tr1.appendChild(th1);
                    tr1.appendChild(th2);
                    tr1.appendChild(th3);
                    tr1.appendChild(th4);
                    tr1.appendChild(th5);
                    tr1.style.border = '1px solid';
                    table.appendChild(tr1);
                    let totalCount = 0;
                    for (const item of element.products) {
                        const tr = document.createElement('tr');

                        const td1 = document.createElement('td');
                        const td2 = document.createElement('td');
                        const td3 = document.createElement('td');
                        const td4 = document.createElement('td');
                        const td5 = document.createElement('td');

                        const rate = item.amount/ item.quantity;
                        totalCount = totalCount + item.amount;
                        const text1 = document.createTextNode(item.productName);
                        const text2 = document.createTextNode(item.packagingType);
                        const text3 = document.createTextNode(item.quantity);
                        const text4 = document.createTextNode(rate.toFixed(2));
                        const text5 = document.createTextNode(item.amount);

                        td1.appendChild(text1);
                        td2.appendChild(text2);
                        td3.appendChild(text3);
                        td4.appendChild(text4);
                        td5.appendChild(text5);

                        td1.style.padding = '8px';
                        td2.style.padding = '8px';
                        td3.style.padding = '8px';
                        td4.style.padding = '8px';
                        td5.style.padding = '8px';

                        tr.appendChild(td1);
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        tr.appendChild(td4);
                        tr.appendChild(td5);
                        tr.style.border = '1px solid';
                        table.appendChild(tr);
                    }
                    const tr2 = document.createElement('tr');
                    const tLast1 = document.createElement('td');
                    const tLast2 = document.createElement('td');
                    const tLast3 = document.createElement('td');
                    const tLast4 = document.createElement('td');
                    const tLast5 = document.createElement('td');

                    const bottom1 = document.createTextNode('Total');
                    const bottom2 = document.createTextNode('');
                    const bottom3 = document.createTextNode('');
                    const bottom4 = document.createTextNode('');
                    const bottom5 = document.createTextNode(totalCount.toFixed(2));

                    tLast1.appendChild(bottom1);
                    tLast2.appendChild(bottom2);
                    tLast3.appendChild(bottom3);
                    tLast4.appendChild(bottom4);
                    tLast5.appendChild(bottom5);

                    tr2.appendChild(tLast1);
                    tr2.appendChild(tLast2);
                    tr2.appendChild(tLast3);
                    tr2.appendChild(tLast4);
                    tr2.appendChild(tLast5);
                    tr2.style.border = '1px solid';
                    table.appendChild(tr2);
                    table.style.margin = '20px';
                    table.style.width = '100%';
                    table.style.border = '1px solid #314456';
                    const replacements = {
                        DATE: new Date().toLocaleString(),
                        WAREHOUSE_NAME: element.warehouseName,
                        WAREHOUSE_EMAIL: element.ownerEmailId,
                        WAREHOUSE_MOB: element.ownerContactNumber,
                        VENDOR_NAME: element.vendorName,
                        TABLE: table.outerHTML
                    };
                    const htmlToSend = template(replacements);
                    // console.log('htmlToSend', htmlToSend);
                    const mailBody: any = {
                        to: element.vendorEmail,
                        cc: element.ownerEmailId,
                        subject: 'Your last month bill',
                        html: htmlToSend,
                    };
                    await new EmailServer().sendEmail(mailBody);
                    // tslint:disable-next-line:no-console
                    // console.log('mail status', mailStatus);
                }
                count = 0;
            }
        });
    };
}