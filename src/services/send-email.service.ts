import PackagingType from '../models/packagingType.model';
import Vendor from '../models/vendor.model';
import Warehouse from '../models/warehouse.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
const { ObjectId } = Types;
import { EmailServer } from '../helpers/EmailService';
import * as fs from 'fs';


export class SendEmailService {
    async sendTransactionEmail(files: any, body: any) {
        const attachments = [];
        for (const element of files) {
            const splitArr = element.originalname.split(".");
			const file = splitArr[0].split(" ").join("_");
			const extention = splitArr[splitArr.length - 1];
			const modifiedFileName = `${file}${new Date().getTime()}.${extention}`;
            const temp = {
                filename: modifiedFileName,
                content: element.buffer,
            }
            attachments.push(temp);
        }
        const mailBody: any = {
            to: body.vendor.emailId, // vendor email
            cc: body.warehouse.ownerEmailId,
            subject: 'Transaction successful',
            html: body.content,
            attachment: attachments
        };
        const mailStatus = await new EmailServer().sendEmail(mailBody);
        return true;
    }
}

