import * as mongoose from 'mongoose';
import { Types } from "mongoose";
import Offer from '../models/offer.model';
const { ObjectId } = Types;

export class OfferService {

    async checkOfferExist(offerName: string): Promise<any> {
        const isOffer = await Offer.findOne({ name: offerName, isDeleted: false });
        if (isOffer) {
            return true;
        } else {
            return false;
        }
    }

    async createOffer(data: any): Promise<any> {
        const offer = new Offer({
            _id: new mongoose.Types.ObjectId(),
            name: data.name,
            period: data.period,
            amount: data.amount
        })
        // tslint:disable-next-line:no-console
        return await offer.save().then(res => {
            // tslint:disable-next-line:no-console
            return res;
        })
        .catch(err => {
            return false;
        });
    }

    /**
     * @description method to fetch offer based on the id
     * @param id offer unique id
     */
    async getOffer(id: string): Promise<any> {
        if (id) {
            const offer = await Offer.find({_id: ObjectId(id), isDeleted: false});
            return offer;
        } else {
            const offer = await Offer.find({ isDeleted: false });
            return offer;
        }
    }

    async updateOffer(data: any): Promise<any> {
        if (data._id) {
            const offer = await Offer.updateOne({ _id: ObjectId(data._id) }, data);
            if (offer.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async deleteOffer(data: any): Promise<any> {
        if (data._id) {
            const offer = await Offer.updateOne({ _id: ObjectId(data._id) }, data);
            if (offer.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

 }