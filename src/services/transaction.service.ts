import * as mongoose from 'mongoose';
import { Types } from "mongoose";
import Transaction from '../models/transaction.model';
import Import from '../models/imports.model';
import FaultyProduct from '../models/faultyProduct.model';
import MonthBill from '../models/monthBill.model';
import Product from '../models/product.model';
import ProductVendorMapping from '../models/productVendorMapping.model';
import Offer from '../models/offer.model';
import { ProductService } from './product.service';
import ProductStorage from '../models/storage.model';
const { ObjectId } = Types;
import delay from 'delay';

export class TransactionService {
    async createTransaction(data: any, faultyId = '') {
        const highestUniqueBillId = await this.getBillId(data.warehouse, data.actionType);
        let transaction;
        if (!data.isFaulty) {
            transaction = new Transaction({
                _id: new mongoose.Types.ObjectId(),
                warehouse: data.warehouse,
                vendor: data.vendor,
                productDetails: data.productDetails,
                DONumber: data.DONumber,
                actionType: data.actionType,
                vehicleNumber: data.vehicleNumber,
                messageBy: data.messageBy,
                driverContactNumber: data.driverContactNumber,
                isFaulty: data.isFaulty,
                uniqueBillId: highestUniqueBillId,
                createdAt: new Date(data.createdAt),
            });
        } else {
            transaction = new Transaction({
                _id: new mongoose.Types.ObjectId(),
                warehouse: data.warehouse,
                vendor: data.vendor,
                productDetails: data.productDetails,
                DONumber: data.DONumber,
                actionType: data.actionType,
                vehicleNumber: data.vehicleNumber,
                messageBy: data.messageBy,
                driverContactNumber: data.driverContactNumber,
                isFaulty: data.isFaulty,
                faultyProduct: faultyId,
                uniqueBillId: highestUniqueBillId,
                createdAt: new Date(data.createdAt),
            });
        }
        const saveTransaction = await transaction.save();
        if (saveTransaction) {
            const transactionResult = await Transaction.findById({ _id: ObjectId(saveTransaction._id) })
                            .populate('warehouse').populate('vendor').populate('faultyProduct');
            return transactionResult;
        } else {
            return false;
        }
    }

    async getBillId(wId: string, actionType: string) {
        const d = new Date().getDate();
        const m = new Date().getMonth();
        const y = new Date().getFullYear();

        const date = new Date(y, m, 1);
        const highestRecord: any = await Transaction.findOne({ warehouse: ObjectId(wId), actionType, createdAt: { "$gte": new Date(date) } })
            .sort({uniqueBillId: 'desc'});
        if (highestRecord) {
            if (highestRecord.uniqueBillId) {
                return highestRecord.uniqueBillId + 1;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    async getTransaction(id: string, wId: string, vId: string, pId: string, paginationObj: any, actionType: string) {
        const query: any = {
            isDeleted: false,
        };
        if (actionType) {
            query.actionType = actionType;
        }
        if (paginationObj.startDate && paginationObj.endDate) {
            query.createdAt = {"$gte": new Date(paginationObj.startDate), "$lte": new Date(paginationObj.endDate)};
        }
        if (id) {
            query._id = ObjectId(id);
        } else if (wId && vId && pId) {
            query.warehouse = ObjectId(wId);
            query.vendor = ObjectId(vId);
            query.productDetails = { $elemMatch: { _id: pId } };
        } else if (wId && vId) {
            query.warehouse = ObjectId(wId);
            query.vendor = ObjectId(vId);
        } else if (wId && pId) {
            query.warehouse = ObjectId(wId);
            query.productDetails = { $elemMatch: { _id: pId } };
        } else if (wId ) {
            query.warehouse = ObjectId(wId);
        } else if (vId) {
            query.vendor = ObjectId(vId);
        }

        if (paginationObj.getAllData) {
            const totalCount = await Transaction.find(query)
            .countDocuments();

            const transaction = await Transaction.find(query)
                .populate('warehouse').populate('vendor').populate('faultyProduct')
                .sort({createdAt: 'asc'});
            return { result: transaction, count: totalCount };
        } else {
            const totalCount = await Transaction.find(query)
            .countDocuments();

            const transaction = await Transaction.find(query)
                .populate('warehouse').populate('vendor').populate('faultyProduct')
                .sort({createdAt: 'desc'})
                .skip(paginationObj.itemsPerPage * (paginationObj.currentPage - 1))
                .limit(paginationObj.itemsPerPage);
            return { result: transaction, count: totalCount };
        }
    }

    async createImport(data: any, tranId: string, productId: string, impQty: number) {
        const importData = new Import({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouse,
            vendor: data.vendor,
            transaction: tranId,
            product: productId,
            importQty: impQty,
            exportQty: 0,
            pendingQty: impQty,
            status: 'Pending',
            DONumber: data.DONumber,
            importDate: new Date(),
            expiryDate: new Date(new Date().setDate(new Date().getDate() + 30)),
        });
        const saveImport = await importData.save();
    }

    async updateImport(data: any, prodId: string, qty: number) {
        const getImportDetails: any = await Import.find({ product: ObjectId(prodId), status: { $ne: 'Completed' }, isDeleted: false }).sort({expiryDate: 'ascending'});
        let temp = qty;
        let qtyUsed;
        const importList = [];
        for (const element of getImportDetails) {
            if (temp > 0) {
                if (parseFloat(element.pendingQty) < temp) {
                    temp = temp - parseFloat(element.pendingQty);
                    qtyUsed = parseFloat(element.pendingQty);
                } else {
                    qtyUsed = temp;
                    temp = 0;
                }
                const exp = parseFloat(element.exportQty) + qtyUsed;
                const pending = parseFloat(element.importQty) - exp;
                const sts =  (pending > 0 ? 'InProcess' : 'Completed');
                const updateObj = {
                    _id: element._id,
                    exportQty: exp,
                    pendingQty: pending,
                    status: sts
                }
                await Import.updateOne({ _id: element._id }, updateObj);
                importList.push({ importId: element._id, quantity: qtyUsed });
            } else {
                break;
            }
        }
        return importList;
    }

    async createFaultyProduct(data: any) {
        const faultyProduct = new FaultyProduct({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouse,
            vendor: data.vendor,
            productDetails: data.faultyProduct,
        });
        const saveFaulty = await faultyProduct.save();
        if (saveFaulty) {
            return saveFaulty;
        } else {
            return false;
        }
    }

    async managePostTransaction(tranId: string, data: any) {
        if ((data.actionType).toLowerCase() === 'import') {
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < data.productDetails.length; i++) {
                await this.createImport(data, tranId, data.productDetails[i]._id, data.productDetails[i].quantity);
                await this.updateMonthBill(data, data.productDetails[i]._id, parseFloat(data.productDetails[i].quantity));
            }
        } else {
            const importList = [];
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < data.productDetails.length; i++) {
                // tslint:disable-next-line:no-console
                console.warn(' 1st in side loop', data.productDetails[i]);
                const temp =  await this.updateImport(data,  data.productDetails[i]._id, parseFloat(data.productDetails[i].quantity));
                importList.push(...temp);
            }
            this.updateTransaction(tranId, importList);
        }
    }

    async updateMonthBill(data: any, prodId: string, qty: number) {
        const getRate: any = await Product.findOne({ _id: ObjectId(prodId), vendor: ObjectId(data.vendor), warehouse: ObjectId(data.warehouse)});
        const getMonthBill: any = await MonthBill.findOne({ product: ObjectId(prodId), vendor: ObjectId(data.vendor), warehouse: ObjectId(data.warehouse) });
        const updateObj = {
            _id: getMonthBill._id,
            quantity: (getMonthBill.quantity) + qty,
            amount: ((getMonthBill.quantity + qty) * parseFloat(getRate.rate))
        }

        const test = await MonthBill.updateOne({ _id: getMonthBill._id}, updateObj);
    }

    async createMonthBill(data: any, prodId: string) {
        const monthBill = new MonthBill({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouseId,
            vendor: data.vendor,
            product: prodId,
            quantity: 0,
            amount: 0,
        });

        const saveMonthBill = await monthBill.save();
    }

    async updateTransaction(transId: string, data: any) {
        const updateObj = {
            _id: transId,
            exportedFrom: data
        };
        await Transaction.updateOne({ _id: ObjectId(transId) }, updateObj);
    }

    async deleteImportTransaction(data: any) {
        const updateImport = await Import.updateMany({ transaction: ObjectId(data._id) }, { isDeleted: true });
        const updateTransaction = await Transaction.updateOne({ _id: ObjectId(data._id) }, { isDeleted: true });
        this.manageMonthBillOnDeleteImport(data);
        this.manageStorageAfterDeleteTransaction(data, 'import');
        if (updateTransaction.nModified > 0) {
            return true;
        } else {
            return false;
        }
    }

    async checkImportStatus(transId: string) {
        const checkSts = await Import.find({ transaction: transId, status: { $in: [ 'InProcess', 'Completed' ] }, isDeleted: false });
        if (checkSts.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    async deleteExportTransaction(data: any) {
        const updateTransaction = await Transaction.updateOne({ _id: ObjectId(data._id) }, { isDeleted: true });
        for (const element of data.exportedFrom) {
            const getImportDetails: any = await Import.findOne({ _id: ObjectId(element.importId), isDeleted: false });
            const checkExportAvailable = await Transaction.find({ exportedFrom: { $elemMatch: { importId: element.importId }}, isDeleted: false });
            const updateObj = {
                _id: getImportDetails._id,
                exportQty: parseFloat(getImportDetails.exportQty) - parseFloat(element.quantity),
                pendingQty: parseFloat(getImportDetails.pendingQty) + parseFloat(element.quantity),
                status: (checkExportAvailable.length > 0) ? 'InProcess' : 'Pending'
            }
            await Import.updateOne({ _id: element.importId }, updateObj);
        }
        if (updateTransaction.nModified > 0) {
            this.manageStorageAfterDeleteTransaction(data, 'export');
            return true;
        } else {
            return false;
        }
    }

    async manageStorageAfterDeleteTransaction(data: any, action: string) {
        const productService = new ProductService();
        for (const element of data.productDetails) {
            const getStorage: any = await ProductStorage.findOne({ _id: ObjectId(element.storageId)});
            if (getStorage) {
                const updateStorage = {
                    _id: element.storageId,
                    quantity: action === 'export' ? (parseFloat(getStorage.quantity) + parseFloat(element.quantity) )
                                : (parseFloat(getStorage.quantity) - parseFloat(element.quantity) )
                }
                productService.updateStorage(updateStorage);
            }
        }
    }

    async deleteFromMonthBill(data: any, prodId: string, qty: number) {
        const getRate: any = await Product.findOne({ _id: ObjectId(prodId), vendor: ObjectId(data.vendor._id), warehouse: ObjectId(data.warehouse._id)});
        const getMonthBill: any = await MonthBill.findOne({ product: ObjectId(prodId), vendor: ObjectId(data.vendor._id), warehouse: ObjectId(data.warehouse._id) });
        const updateObj = {
            _id: getMonthBill._id,
            quantity: (getMonthBill.quantity) - qty,
            amount: ((getMonthBill.quantity - qty) * parseFloat(getRate.rate))
        }

        const test = await MonthBill.updateOne({ _id: getMonthBill._id}, updateObj);
    }

    async manageMonthBillOnDeleteImport(data: any) {
        const importArray: any = await Import.find({ transaction: ObjectId(data._id) });
        for (const element of importArray) {
            await this.deleteFromMonthBill(data, element.product, element.importQty);
        }
    }

}