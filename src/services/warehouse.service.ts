import Warehouse from '../models/warehouse.model';
import { Types } from "mongoose";
const { ObjectId } = Types;
import * as mongoose from 'mongoose';

export class WarehouseService {
    async createWarehouse(data: any, authId: string): Promise<any> {
        const warehouse = new Warehouse({
            _id: new mongoose.Types.ObjectId(),
            auth: authId,
            name: data.name,
            ownerName: data.ownerName,
            ownerContactNumber: data.ownerContactNumber,
            ownerEmailId: data.ownerEmailId,
            address: data.address
        });
        return await warehouse.save().then(warehouseRes => {
            if (warehouseRes) {
                return warehouseRes;
            } else {
                return false;
            }
        });
    }

    async getWarehouse(id?: string, authId?: string): Promise<any> {
        if (id && authId) {
            const warehouse = await Warehouse.find({_id: ObjectId(id), isDeleted: false, auth: ObjectId(authId)});
            return warehouse;
        } else if (id) {
            const warehouse = await Warehouse.find({_id: ObjectId(id), isDeleted: false});
            return warehouse;
        } else if (authId) {
            const warehouse = await Warehouse.find({auth: ObjectId(authId), isDeleted: false});
            return warehouse;
        } else {
            const warehouse = await Warehouse.find({ isDeleted: false });
            return warehouse;
        }
    }

    async updateWarehouse(data: any): Promise<any> {
        if (data._id) {
            const warehouse = await Warehouse.updateOne({ _id: ObjectId(data._id) }, data);
            if (warehouse.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async deleteWarehouse(data: any): Promise<any> {
        if (data._id) {
            const warehouse = await Warehouse.updateOne({ _id: ObjectId(data._id) }, data);
            if (warehouse.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}