import PackagingType from '../models/packagingType.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
const { ObjectId } = Types;
import { UnauthorizedError, InternalServerError, NotFoundError } from 'routing-controllers';

export class PackagingTypeService {

    async createPackagingType(data: any): Promise<any> {
        const packagingType = new PackagingType({
            _id: new mongoose.Types.ObjectId(),
            warehouse: data.warehouseId,
            packagingType: data.packagingType
        })
        // tslint:disable-next-line:no-console
        return await packagingType.save().then(res => {
            // tslint:disable-next-line:no-console
            return res;
        })
        .catch(err => {
            return false;
        });
    }

    async checkPackagingTypeExist(data: any): Promise<any> {
        const packageType = await PackagingType.findOne({ packagingType: data.packagingType, warehouse: ObjectId(data.warehouseId), isDeleted: false });
        if (packageType) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @description method to fetch packaging type based on the id
     * @param id packaging type unique id
     */
    async getPackagingType(id: string, wId: string): Promise<any> {
        if (id) {
            const packagingType = await PackagingType.find({_id: ObjectId(id), isDeleted: false}).sort({createdAt: 'desc'});
            return packagingType;
        } else if (wId) {
            const packagingType = await PackagingType.find({ warehouse: wId, isDeleted: false }).sort({createdAt: 'desc'});
            return packagingType;
        } else {
            const packagingType = await PackagingType.find({ isDeleted: false }).sort({createdAt: 'desc'});
            return packagingType;
        }
    }

    async updatePackagingType(data: any): Promise<any> {
        if (data._id) {
            const packagingType = await PackagingType.updateOne({ _id: ObjectId(data._id) }, data);
            if (packagingType.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async deletePackagingType(data: any): Promise<any> {
        if (data._id) {
            const packagingType = await PackagingType.updateOne({ _id: ObjectId(data._id) }, data);
            if (packagingType.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
 }