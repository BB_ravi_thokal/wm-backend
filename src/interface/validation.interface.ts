import { IsEmail, IsNotEmpty, IsPhoneNumber, IsMobilePhone, isMobilePhone } from 'class-validator'

// tslint:disable-next-line:max-classes-per-file
export class Warehouse {
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    password: string

    @IsNotEmpty()
    role: string

    @IsNotEmpty()
    ownerName: string

    @IsMobilePhone(null)
    ownerContactNumber: string

    @IsEmail()
    ownerEmailId: string

    @IsNotEmpty()
    address: string
}

// tslint:disable-next-line:max-classes-per-file
export class UpdateWarehouse {
    _id?: string
    auth?: string
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    ownerName: string

    @IsMobilePhone(null)
    ownerContactNumber: string

    @IsEmail()
    ownerEmailId: string

    @IsNotEmpty()
    address: string

    isActive?: boolean
    subscription?: string
    subscriptionEndDate?: Date
    isDeleted?: boolean
}

// tslint:disable-next-line:max-classes-per-file
export class Vendor {
    _id?: string
    @IsNotEmpty()
    warehouseId: string

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    @IsEmail()
    emailId: string

    @IsNotEmpty()
    role: string

    @IsMobilePhone(null)
    contactNumber: string

    TotalAmountPaid?: number
    pendingDues?: number
}

// tslint:disable-next-line:max-classes-per-file
export class UpdateVendor {
    _id?: string
    auth?: string

    @IsNotEmpty()
    warehouse: string

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    @IsEmail()
    emailId: string

    @IsMobilePhone(null)
    contactNumber: string

    TotalAmountPaid?: number
    pendingDues?: number
    isDeleted?: boolean
}

// tslint:disable-next-line:max-classes-per-file
export class PackagingType {
    _id?: string

    @IsNotEmpty()
    warehouseId: string

    @IsNotEmpty()
    packagingType: string
}

// tslint:disable-next-line:max-classes-per-file
export class Product {
    _id?: string

    @IsNotEmpty()
    warehouseId: string

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    packagingTypeId: string

    @IsNotEmpty()
    rate: number

    @IsNotEmpty()
    vendor: string

}

// tslint:disable-next-line:max-classes-per-file
export class PStorage {
    _id: string

    quantity: number
}

// tslint:disable-next-line:max-classes-per-file
export class Offer {
    _id?: string

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    amount: number

    @IsNotEmpty()
    period: number

    isDeleted?: false
}

// tslint:disable-next-line:max-classes-per-file
export class Subscription {
    _id?: string

    @IsNotEmpty()
    warehouse: string

    @IsNotEmpty()
    offer: string

    @IsNotEmpty()
    paymentMode: string

    @IsNotEmpty()
    isActive: boolean
}

// tslint:disable-next-line:max-classes-per-file
export class Transaction {
    _id?: string

    @IsNotEmpty()
    warehouse: string

    @IsNotEmpty()
    vendor: string

    @IsNotEmpty()
    productDetails: any[]

    DONumber?: string

    @IsNotEmpty()
    actionType: string

    @IsNotEmpty()
    vehicleNumber: string

    messageBy?: string

    @IsNotEmpty()
    driverContactNumber: string

    isFaulty?: boolean
    faultyProduct?: any[]
}

// tslint:disable-next-line:max-classes-per-file
export class Login {
    @IsNotEmpty()
    userId: string

    @IsNotEmpty()
    password: string
}

// tslint:disable-next-line:max-classes-per-file
export class ForgotPassword {
    @IsEmail()
    emailId: string
}

// tslint:disable-next-line:max-classes-per-file
export class ChangePassword {
    @IsEmail()
    emailId: string

    @IsNotEmpty()
    temporaryPassword: string

    @IsNotEmpty()
    newPassword: string
}