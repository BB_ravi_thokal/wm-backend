import {
  Middleware,
  ExpressErrorMiddlewareInterface
} from "routing-controllers";
import { Response } from "express";
import { isArray } from "class-validator";
import { logger } from "../logger";

export class CustomErrorHandler implements ExpressErrorMiddlewareInterface {
  error(error: any, request: any, response: Response, next: any) {
    const statusCode = !!error.httpCode ? error.httpCode : response.statusCode;
    let errorObj;
    // handle class-validator error and send responce accordingly
    if (!!error.errors && isArray(error.errors)) {
      const validationErrors = error.errors.reduce((acc: any[], currentElement: any) => {

        // for children object validation
        if (currentElement.children.length > 0 && isArray(currentElement.children)) {
          acc.push(...processChildErrors(currentElement.children, currentElement.property))
        } else {
          acc.push({
            parent_property: null,
            property: currentElement.property,
            constraints: Object.values(currentElement.constraints).join(" & ")
          })
        }

        return acc;
      }, [])

      errorObj = {
        statusCode,
        date: Date.now(),
        message: validationErrors,
        request_url: request.url,
        request_data: request.method === "GET" ? request.query || request.params : request.body,
        trace: error.stack
      }
      return response.status(statusCode).json(validationErrors)

    }

    errorObj = {
      statusCode,
      date: Date.now(),
      message: error.message,
      request_url: request.url,
      request_data: request.method === "GET" ? request.query || request.params : request.body,
      trace: error.stack
    }
    logger.error(errorObj);

    const Error = {
      status: false,
      message: error.message,
    }

    // default response
    response.status(statusCode).json(Error);
  }
}

const processChildErrors = (errArr: any[], parent?: any) => {

  const errorMessageArr = errArr.reduce((acc: any[], current: any) => {

    if (current.children.length > 0) {
      acc.push(...processChildErrors(current.children, parent))
    } else {
      acc.push({
        parent_property: parent,
        property: current.property,
        message: Object.values(current.constraints).join(" & ")
      })
    }
    return acc;
  }, [])

  return errorMessageArr;
}